package com.nakov.courses.java.fundamentals.lection5.homework.problem2.products.childs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.nakov.courses.java.fundamentals.lection5.homework.problem2.Product;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.interfaces.Expirable;

public class FoodProduct extends Product implements Expirable {

	private boolean exprired;
	
	public FoodProduct(String name, double price, int quantity, AgeRestriction adult) {
		super(name, price, quantity, adult);
	}
	
	@Override
	public Date getExpirationDate() {
		// add 30 days expiration date
		Calendar cal = Calendar.getInstance(); 
		cal.add(Calendar.MONTH, 1);
		return cal.getTime();
	}
	@Override
	public double getPrice() {
		int days = (int)Math.round(getExpirationDate().getTime() - new Date().getTime());
		if (days <=15 ) {
			return getPrice() * 0.7;
		}
		return super.getPrice();
	}

	public boolean isExprired() {
		return getExpirationDate().getTime() < new Date().getTime();
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " name: "  +  getName() + ", price: " + getPrice() + ", quantity: " + getQuatity() + ", Adult: " + getAgeRestriction() + ", expirationDate: " + new SimpleDateFormat("dd/MM/YYYY").format(getExpirationDate()) + ", price: " + getPrice();
	}
}
