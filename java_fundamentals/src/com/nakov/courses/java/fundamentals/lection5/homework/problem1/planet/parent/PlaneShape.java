package com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.parent;

import java.util.List;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.Shape;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.interfaces.AreaMeasurable;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.interfaces.PerimeterMeasurable;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.model.Vertices2D;

public abstract class PlaneShape extends Shape implements PerimeterMeasurable, AreaMeasurable{
	private List<Vertices2D> listVersices;
	
	public PlaneShape(List<Vertices2D> listVersices) {
		this.listVersices = listVersices;
	}

	public List<Vertices2D> getListVersices() {
		return listVersices;
	}
}
