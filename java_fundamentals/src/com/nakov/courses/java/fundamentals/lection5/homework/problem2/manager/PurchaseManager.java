package com.nakov.courses.java.fundamentals.lection5.homework.problem2.manager;

import com.nakov.courses.java.fundamentals.lection5.homework.problem2.Customer;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.Product;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions.BuyerNoPermissionException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions.BuyerOutOfMoneyException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions.OutOfStockException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions.ProductExpiredException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.products.childs.FoodProduct;

public class PurchaseManager {
	public static void processPurchase(Customer customer, Product product) throws OutOfStockException, BuyerOutOfMoneyException, BuyerNoPermissionException, ProductExpiredException {
		if (product.getQuatity() <= 0) {
			throw new OutOfStockException("Out of quantity for product: " + product.getName());
		}
		
		if ((product instanceof FoodProduct) && (((FoodProduct)product).isExprired())) {
			throw new ProductExpiredException("Product " + product.getName() + " has expired");
		}
		
		if (customer.getBalance() < product.getPrice() * product.getQuatity()) {
			throw new BuyerOutOfMoneyException("Customer: " + customer.getName() + ", does not have enought money for product: " + product.getName());
		}
		
		if (customer.getAge() < 18) {
			throw new BuyerNoPermissionException("You are too young to buy this product!");
		}
		
		product.setQuatity(product.getQuatity() - 1);
		customer.setBalance(customer.getBalance() - product.getPrice()*product.getQuatity());
	} 
}
