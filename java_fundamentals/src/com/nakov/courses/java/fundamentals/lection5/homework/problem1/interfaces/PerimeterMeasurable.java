package com.nakov.courses.java.fundamentals.lection5.homework.problem1.interfaces;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.exceptions.WrongNumberVersicesException;

public interface PerimeterMeasurable {
	public double getPerimeter() throws WrongNumberVersicesException;
}
