package com.nakov.courses.java.fundamentals.lection5.homework.problem1.interfaces;

public interface AreaMeasurable {
	public double getArea();
}
