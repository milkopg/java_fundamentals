package com.nakov.courses.java.fundamentals.lection5.homework.problem2.products.childs;

import com.nakov.courses.java.fundamentals.lection5.homework.problem2.products.ElectonicsProduct;

public class Computer extends ElectonicsProduct{

	public Computer(String name, double price, int quatity, AgeRestriction ageRestriction, int guaranteePeriod) {
		super(name, price, quatity, ageRestriction, guaranteePeriod);
	}

	@Override
	public double getPrice() {
		return super.getQuatity() > 1000 ? super.getPrice() * 0.95 : super.getPrice(); 
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " name: "  +  getName() + ", price: " + getPrice() + ", quantity: " + getQuatity() + ", Adult: " + getAgeRestriction() + ", guarantiePeriod: " + getGuaranteePeriod() + ", price: " + getPrice();
	}
}
