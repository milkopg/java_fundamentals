package com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions;

public class OutOfStockException extends Exception {
	private static final long serialVersionUID = 586195370267538151L;
	
	public OutOfStockException(String message) {
		super(message);
	}
}
