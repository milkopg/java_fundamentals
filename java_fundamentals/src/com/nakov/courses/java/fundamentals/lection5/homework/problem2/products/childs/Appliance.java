package com.nakov.courses.java.fundamentals.lection5.homework.problem2.products.childs;

import java.util.Date;

import com.nakov.courses.java.fundamentals.lection5.homework.problem2.products.ElectonicsProduct;

public class Appliance extends ElectonicsProduct {

	public Appliance(String name, double price, int quatity, AgeRestriction ageRestriction, int guaranteePeriod) {
		super(name, price, quatity, ageRestriction, guaranteePeriod);
	}
	
	@Override
	public double getPrice() {
		return super.getQuatity() < 50 ? super.getPrice()*1.05 : super.getPrice();
	}
}
