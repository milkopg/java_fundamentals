package com.nakov.courses.java.fundamentals.lection5.homework.problem2.products;

import com.nakov.courses.java.fundamentals.lection5.homework.problem2.Product;

public abstract class ElectonicsProduct extends Product{
	int guaranteePeriod;
	
	public int getGuaranteePeriod() {
		return guaranteePeriod;
	}

	public void setGuaranteePeriod(int guaranteePeriod) {
		this.guaranteePeriod = guaranteePeriod;
	}

	public ElectonicsProduct(String name, double price, int quatity, AgeRestriction ageRestriction, int guaranteePeriod2) {
		super(name, price, quatity, ageRestriction);
		setGuaranteePeriod(guaranteePeriod2);
	}

}
