package com.nakov.courses.java.fundamentals.lection5.homework.problem1.exceptions;

public class WrongNumberVersicesException extends IllegalArgumentException{
		/**
	 * 
	 */
	private static final long serialVersionUID = -2232652465272769157L;

		public WrongNumberVersicesException(String message) {
			super(message) ;
		}
}
