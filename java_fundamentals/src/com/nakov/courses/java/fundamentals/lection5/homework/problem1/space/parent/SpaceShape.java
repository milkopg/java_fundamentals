package com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.parent;

import java.util.List;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.Shape;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.interfaces.AreaMeasurable;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.interfaces.VolumeMeasurable;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.model.Vertices3D;

public abstract class SpaceShape extends Shape implements AreaMeasurable, VolumeMeasurable{
	private List<Vertices3D> listVertices3D;

	public SpaceShape(List<Vertices3D> listVertices3D) {
		this.listVertices3D = listVertices3D;
	}

	public List<Vertices3D> getListVertices3D() {
		return listVertices3D;
	}

	public void setListVertices3D(List<Vertices3D> listVertices3D) {
		this.listVertices3D = listVertices3D;
	}
}
