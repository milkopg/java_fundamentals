package com.nakov.courses.java.fundamentals.lection5.homework.problem2.interfaces;

public interface Buyable {
	public double getPrice();
}
