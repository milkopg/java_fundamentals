package com.nakov.courses.java.fundamentals.lection5.homework.problem1.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.Shape;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.childs.Circle;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.childs.Rectangle;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.childs.Triangle;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.model.Vertices2D;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.parent.PlaneShape;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.childs.Cuboid;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.childs.Sphere;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.childs.SquarePyramid;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.model.Vertices3D;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.parent.SpaceShape;

public class Problem1Main {

	public static void main(String[] args) {
		Shape [] shapes = new Shape[6];
		List<Vertices2D> listVersices2D = new ArrayList<Vertices2D>();
		
		listVersices2D = new ArrayList<Vertices2D>();
		listVersices2D.add(new Vertices2D(2, -1));
		Circle circle = new Circle(listVersices2D, 5);
		shapes[0] = circle;
		
		listVersices2D = new ArrayList<Vertices2D>();
		//listVersices2D.add(new Vertices2D(5, 5, 2, -1));
		listVersices2D.add(new Vertices2D(2, -1));
		Rectangle rectangle = new Rectangle(listVersices2D, 20, 10);
		shapes[1] = rectangle;
		
		listVersices2D = new ArrayList<Vertices2D>();
		listVersices2D.add(new Vertices2D(-2, 1));
		listVersices2D.add(new Vertices2D(3, 2));
		listVersices2D.add(new Vertices2D(-4, 5));
		Triangle triangle = new Triangle(listVersices2D);
		shapes[2] = triangle;
		
		List<Vertices3D> listVersices3D = new ArrayList<>();
		listVersices3D.add(new Vertices3D(0, 0, 0));
		Cuboid cuboid = new Cuboid(listVersices3D, 8, 5, 6);
		shapes[3] = cuboid;
		
		Sphere sphere = new Sphere(listVersices3D, 5);
		shapes[4] = sphere;
		
		SquarePyramid squarePyramid = new SquarePyramid(listVersices3D, 10, 5);
		shapes[5] = squarePyramid;
		
		for (int i=0; i < shapes.length; i++)  {
			System.out.println(shapes[i]);
		}
		System.out.println("-------------------------------");
		System.out.println("Shapes with volumes over 40.00");
        Arrays.asList(shapes).stream().filter(x -> x instanceof SpaceShape).map(x -> (SpaceShape) x).filter(x -> x.getVolume() > 40).forEach(System.out::println);
		System.out.println("-------------------------------");
		System.out.println("Plane Shapes ordered by perimeter");
		Arrays.asList(shapes).stream().filter(x -> x instanceof PlaneShape).map(x -> (PlaneShape) x).sorted((x1, x2) -> Double.compare(x1.getPerimeter() , x2.getPerimeter())).forEach(System.out::println);
	}
}
