package com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.childs;

import java.util.List;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.exceptions.WrongNumberVersicesException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.model.Vertices3D;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.parent.SpaceShape;

public class SquarePyramid extends SpaceShape{
	private double width;
	private double heigth;

	public SquarePyramid(List<Vertices3D> listVertices3D, double width, double heigth) {
		super(listVertices3D);
		if ((getListVertices3D() == null) || (getListVertices3D().isEmpty()) || (getListVertices3D().size() != 1)) {
			throw new WrongNumberVersicesException( getClass().getSimpleName() + " has exact 1 vertice");
		}
		this.width = width;
		this.heigth = heigth;
	}

	public double getWidth() {
		return width;
	}

	public double getHeigth() {
		return heigth;
	}

	@Override
	public double getArea() {
		return Math.pow(getWidth(),2) + 2*getWidth()*Math.sqrt(Math.pow(getWidth(), 2)/4 + Math.pow(getHeigth(), 2));
	}

	@Override
	public double getVolume() {
		return Math.pow(getWidth(), 2) * getHeigth() / 3;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + ", coordinates: " + getListVertices3D() + ", area: " + getArea() + ", volume: " + getVolume();
	}
}
