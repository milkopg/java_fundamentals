package com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions;

public class BuyerNoPermissionException extends Exception {
	private static final long serialVersionUID = -6460777600582902219L;

	public BuyerNoPermissionException(String message) {
		super(message);
	}

}
