package com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.model;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.Vertice;

public class Vertices2D extends Vertice{
	private int x;
	private int y;
	
	public Vertices2D(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	
	@Override
	public String toString() {
		return "Vertices2D x:" + getX() + ", y:" + getY();
	}
}
