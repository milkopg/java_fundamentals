package com.nakov.courses.java.fundamentals.lection5.homework.problem2;

import com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions.InvalidAgeRescrictionTypeException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.interfaces.Buyable;

public abstract class Product implements Buyable{
	private String name;
	private double price;
	private int quatity;
	private AgeRestriction ageRestriction;
	
	public enum AgeRestriction {
	    None, Teenager, Adult;
	}
	
	public Product(String name, double price, int quatity, AgeRestriction adultType) {
		this.name = name;
		this.price = price;
		this.quatity = quatity;
		boolean foundAdultType = false;
		for (AgeRestriction restriction : AgeRestriction.values()) {
			if (restriction.equals(adultType)) {
				foundAdultType = true;
				this.ageRestriction = adultType;
			}
		}
		if  (!foundAdultType) {
			throw new InvalidAgeRescrictionTypeException("No valid age restriction! Possible are: None, Teenager, Adult" );
		}
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQuatity() {
		return quatity;
	}
	public void setQuatity(int quatity) {
		this.quatity = quatity;
	}
	public AgeRestriction getAgeRestriction() {
		return ageRestriction;
	}
	public void setAgeRestriction(AgeRestriction ageRestriction) {
		this.ageRestriction = ageRestriction;
	}
}
