package com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions;

public class BuyerOutOfMoneyException extends Exception {
	private static final long serialVersionUID = -8127053919162626283L;

	public BuyerOutOfMoneyException(String message) {
		super(message);
	}
}
