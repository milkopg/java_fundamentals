package com.nakov.courses.java.fundamentals.lection5.homework.problem2.interfaces;

import java.util.Date;

public interface Expirable {
	public Date getExpirationDate();
}
