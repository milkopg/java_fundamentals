package com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.childs;

import java.util.List;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.exceptions.WrongNumberVersicesException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.model.Vertices3D;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.parent.SpaceShape;

public class Cuboid extends SpaceShape{

	private double width;
	private double height; 
	private double depth;
	
	public Cuboid(List<Vertices3D> listVertices3D, double width, double heigth, double depth) {
		super(listVertices3D);
		if ((getListVertices3D() == null) || (getListVertices3D().isEmpty()) || (getListVertices3D().size() != 1)) {
			throw new WrongNumberVersicesException( getClass().getSimpleName() + " has exact 1 vertice");
		}
		this.width = width;
		this.height = heigth;
		this.depth = depth;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	public double getDepth() {
		return depth;
	}

	@Override
	public double getArea() {
		return 2 * (getWidth()* getDepth() + getDepth()*getHeight() + getHeight()*getWidth());
	}

	@Override
	public double getVolume() {
		return getWidth() * getHeight() * getDepth();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ", coordinates: " + getListVertices3D() + ", area: " + getArea() + ", volume: " + getVolume();
	}
}
