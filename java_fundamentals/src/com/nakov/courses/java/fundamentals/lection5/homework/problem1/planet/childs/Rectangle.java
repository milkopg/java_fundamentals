package com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.childs;

import java.util.List;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.exceptions.WrongNumberVersicesException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.model.Vertices2D;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.parent.PlaneShape;

public class Rectangle extends PlaneShape{

	private double witdh;
	private double height;
	
	public Rectangle(List<Vertices2D> listVersices, int width, int height) {
		super(listVersices);
		if ((getListVersices() == null) || (getListVersices().isEmpty()) || (getListVersices().size() != 1)) {
			throw new WrongNumberVersicesException( getClass().getSimpleName() + " has exact 1 vertice");
		}
		this.witdh = width;
		this.height = height;
	}

	public double getWitdh() {
		return witdh;
	}

	public double getHeight() {
		return height;
	}

	@Override
	public double getPerimeter() {
		return (getWitdh() + getHeight())*2;
	}

	@Override
	public double getArea() {
		return getWitdh()*getHeight();
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "  coordinates: " + getListVersices() + " witdh: " + getWitdh() +  ", heigth: " + getHeight() + ", perimeter: " + getPerimeter() + " , area: " + getArea(); 
	}
}
