package com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.childs;

import java.util.List;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.exceptions.WrongNumberVersicesException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.model.Vertices2D;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.parent.PlaneShape;

public class Circle extends PlaneShape{

	private double radius;
	
	public Circle(List<Vertices2D> listVersices, double radius) {
		super(listVersices);
		if ((getListVersices() == null) || (getListVersices().isEmpty()) || (getListVersices().size() != 1)) {
			throw new WrongNumberVersicesException( getClass().getSimpleName() + " has exact 1 vertice");
		}
		this.radius = radius;
		
	}

	public double getRadius() {
		return radius;
	}

	@Override
	public double getPerimeter() {
		return 2 * Math.PI * getRadius();
	}

	@Override
	public double getArea() {
		return Math.PI * getRadius() * getRadius();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() +  " coordinates: " + getListVersices() + " radius: " + getRadius() + ", perimeter: " + getPerimeter() + " , area: " + getArea(); 
	}
}
