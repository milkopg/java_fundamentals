package com.nakov.courses.java.fundamentals.lection5.homework.problem2.main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.nakov.courses.java.fundamentals.lection5.homework.problem2.Customer;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.Product;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.Product.AgeRestriction;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.products.childs.Appliance;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.products.childs.Computer;
import com.nakov.courses.java.fundamentals.lection5.homework.problem2.products.childs.FoodProduct;

public class Problem2Main {

	public static void main(String[] args) throws Exception {
		List<Product> products = new ArrayList<>();
		
		Product chair = new Appliance("chair", 100, 50, AgeRestriction.None, 6);
		products.add(chair);
		
		Product table = new Appliance("table", 200, 100, AgeRestriction.None, 6);
		products.add(table);
		
		Product computer = new Computer("desktop", 1000, 10, AgeRestriction.Adult, 24);
		products.add(computer);
		
		Product notebook = new Computer("notebook", 1500, 15, AgeRestriction.Adult, 24);
		products.add(notebook);
		
		FoodProduct cigars = new FoodProduct("420 Blaze it fgt", 6.90, 1400, AgeRestriction.Adult);
		products.add(cigars);
		
		FoodProduct chocolate = new FoodProduct("Chocolate Milka", 2 , 100, AgeRestriction.Teenager);
		products.add(chocolate);
		
		System.out.println("Expirable products and get the name of the first product with the soonest date of expiration");
		products.stream().filter(x -> x instanceof FoodProduct).map(x -> (FoodProduct) x).sorted((a, b) -> Long.compare(a.getExpirationDate().getTime(), b.getExpirationDate().getTime())).limit(1).forEach(System.out::println);
		System.out.println("\n\n");
		
		System.out.println("All products with adult age restriction and sort them by price in ascending order");
		products.stream().filter(x -> x.getAgeRestriction().equals(AgeRestriction.Adult)).sorted((a, b) -> Double.compare(a.getPrice(), b.getPrice())).forEach(System.out::println);
		
		Customer pecata = new Customer("Pecata", 17, 30.00);
		Customer barovetz = new Customer("Barovetz", 30, 1000);
//		PurchaseManager.processPurchase(pecata, cigars);
//		
//		Customer gopeto = new Customer("Gopeto", 18, 0.44);
//		PurchaseManager.processPurchase(gopeto, cigars);
	}

}
