package com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.childs;

import java.util.List;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.exceptions.WrongNumberVersicesException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.model.Vertices2D;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.planet.parent.PlaneShape;

public class Triangle extends PlaneShape {
		private Vertices2D a = getListVersices().get(0);
		private Vertices2D b = getListVersices().get(1);
		private Vertices2D c = getListVersices().get(2);
		
	
		public Triangle(List<Vertices2D> listVersices) {
		super(listVersices);
		if ((getListVersices() == null) || (getListVersices().isEmpty()) || (getListVersices().size() != 3)) {
			throw new WrongNumberVersicesException( getClass().getSimpleName() + " has exact 3 vertices");
		}
	}

	@Override
	public double getPerimeter() {
		return getSideAB() + getSideBC() + getSideBC();
	}
	
	public double getSideAB() {
		return Math.sqrt(Math.pow((b.getX() - a.getX()), 2) + Math.pow(b.getY() - a.getY(), 2));
	}

	public double getSideBC() {
		return Math.sqrt(Math.pow((c.getX() - b.getX()), 2) + Math.pow(c.getY() - b.getY(), 2));
	}

	public double getSideAC() {
		return Math.sqrt(Math.pow(c.getX() - a.getX(), 2) + Math.pow(c.getY() - a.getY(), 2));
	}

	@Override
	public double getArea() {
		double area = 0.0;
		double sideA = getSideAB();
		double sideB = getSideBC();
		double sideC = getSideAC();
		
		area = Math.sqrt(getPerimeter()*(getPerimeter() - sideA) * (getPerimeter()-sideB) * (getPerimeter() - sideC));
		return area;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "  coordinates: " + getListVersices() +", perimeter: " + getPerimeter() + " , area: " + getArea(); 
	}
}
