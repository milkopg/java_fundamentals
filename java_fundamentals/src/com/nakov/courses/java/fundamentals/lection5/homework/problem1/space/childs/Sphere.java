package com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.childs;

import java.util.List;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.exceptions.WrongNumberVersicesException;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.model.Vertices3D;
import com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.parent.SpaceShape;

public class Sphere extends SpaceShape{
	private double radius;
	
	public Sphere(List<Vertices3D> listVertices3D, double radius) {
		super(listVertices3D);
		if ((getListVertices3D() == null) || (getListVertices3D().isEmpty()) || (getListVertices3D().size() != 1)) {
			throw new WrongNumberVersicesException( getClass().getSimpleName() + " has exact 1 vertice");
		}
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	@Override
	public double getArea() {
		return 4 * Math.PI * Math.pow(getRadius(), 2);
	}

	@Override
	public double getVolume() {
		return 4 * Math.PI * Math.pow(getRadius(), 3) / 3;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ", coordinates: " + getListVertices3D() + ", area: " + getArea() + ", volume: " + getVolume();
	}
}
