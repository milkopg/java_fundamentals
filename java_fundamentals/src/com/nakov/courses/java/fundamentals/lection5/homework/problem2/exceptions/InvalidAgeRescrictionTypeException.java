package com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions;

public class InvalidAgeRescrictionTypeException extends IllegalArgumentException {
	private static final long serialVersionUID = -2063020488412546825L;

	public InvalidAgeRescrictionTypeException(String  message) {
		super(message);
	}
}
