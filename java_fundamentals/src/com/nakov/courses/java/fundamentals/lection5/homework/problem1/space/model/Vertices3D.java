package com.nakov.courses.java.fundamentals.lection5.homework.problem1.space.model;

import com.nakov.courses.java.fundamentals.lection5.homework.problem1.Vertice;

public class Vertices3D extends Vertice{
	private int x;
	private int y;
	private int z;
	
	public Vertices3D(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " , getX(): " + getX() + ", getY():" + getY() + ", getZ():" + getZ();
	}
}
