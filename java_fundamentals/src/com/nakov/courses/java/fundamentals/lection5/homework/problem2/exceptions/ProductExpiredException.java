package com.nakov.courses.java.fundamentals.lection5.homework.problem2.exceptions;

public class ProductExpiredException extends Exception {
	private static final long serialVersionUID = -5618441478730969918L;
	
	public ProductExpiredException(String message) {
		super(message);
	}
}
