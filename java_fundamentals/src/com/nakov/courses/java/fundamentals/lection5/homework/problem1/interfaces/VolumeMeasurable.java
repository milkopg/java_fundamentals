package com.nakov.courses.java.fundamentals.lection5.homework.problem1.interfaces;

public interface VolumeMeasurable {
	public double getVolume();
}
