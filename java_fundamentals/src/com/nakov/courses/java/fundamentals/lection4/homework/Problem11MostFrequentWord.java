package com.nakov.courses.java.fundamentals.lection4.homework;

import java.io.ObjectInputStream.GetField;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Problem11MostFrequentWord {
	public static void main(String[] args) {
		mostFrequentWord();
	}

	private static void mostFrequentWord() {
		Scanner scanner = new Scanner(System.in);
		String[] words = scanner.nextLine().trim().toLowerCase().split("[^a-zA-Z]+");
		Map<String, Integer> wordsMap = new HashMap<String, Integer>();
		Integer count = 1;
		
		for (int i=0; i < words.length ; i++) {
			if (wordsMap.containsKey(words[i])) {
				count = wordsMap.get(words[i]) + 1;
			} else {
				count = 1;
			}
			wordsMap.put(words[i], count);
		}
		
		for (Entry<String, Integer> entry : wordsMap.entrySet()) {
			if (entry.getValue().equals(Collections.max(wordsMap.values()))) {
				System.out.printf("%s -> %d times\n", entry.getKey(), entry.getValue());
			}
		}
	}
}
