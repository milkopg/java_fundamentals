package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Scanner;

public class Problem16CalculdateFactorial {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(factorial (scanner.nextInt()));
	}

	private static int factorial(int num) {
		if (num == 0) {
			return 1;
		} else {
			return num * factorial(num - 1);
		}
	}
}
