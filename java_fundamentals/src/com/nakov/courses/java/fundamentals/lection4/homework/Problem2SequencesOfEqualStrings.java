package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class Problem2SequencesOfEqualStrings {

	public static void main(String[] args) {
		sequenceOfStrings();
	}

	private static void sequenceOfStrings() {
		Scanner scanner = new Scanner(System.in);
		
		String [] lines = scanner.nextLine().split(" ");
		LinkedHashMap<String, Integer> wordsMap = new LinkedHashMap<>();
		for (int i=0; i < lines.length; i++) {
			if (wordsMap.containsKey(lines[i])) {
				wordsMap.put(lines[i], wordsMap.get(lines[i]) + 1);
			} else {
				wordsMap.put(lines[i], 1);
			}
		}
		printElements(wordsMap);
	}

	private static void printElements(LinkedHashMap<String, Integer> wordsMap) {
		for (String key : wordsMap.keySet()) {
			for (int i =0; i < wordsMap.get(key); i++) {
				System.out.print(key + " ");
			}
			System.out.println();
		}
	}
}
