package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class Problem3LargestSequenceOfEqualStrings {

	public static void main(String[] args) {
		findLargestSequence();
	}
	
	private static void findLargestSequence() {
		Scanner scanner = new Scanner(System.in);
		
		String [] lines = scanner.nextLine().split(" ");
		LinkedHashMap<String, Integer> wordsLhm = new LinkedHashMap<>();
		for (int i=0; i < lines.length; i++) {
			if (wordsLhm.containsKey(lines[i])) {
				wordsLhm.put(lines[i], wordsLhm.get(lines[i]) + 1);
			} else {
				wordsLhm.put(lines[i], 1);
			}
		}
		printElements(wordsLhm);
	}

	private static void printElements(LinkedHashMap<String, Integer> wordsMap) {
		int maxValue = 0;
		String largestWord = "";
		for (String key : wordsMap.keySet()) {
			if (wordsMap.get(key) > maxValue) {
				maxValue = wordsMap.get(key); 
				largestWord = key;
			}
		}
		
		for (int i=0; i < maxValue; i++) {
			System.out.print(largestWord + " ");
		}
	}

}