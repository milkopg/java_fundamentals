package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class _2SequencesEqualStrings {
	
//	Write a program that enters an array of strings and finds in it 
//	all sequences of equal elements. 
//	The input strings are given as a single line, separated by a space. 

	public static void main(String[] args) {
		

		List<Character> list1, list2;
		List<Character> output = new ArrayList<Character>();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first list of letters :");
		list1 = getListOfLetters(sc.nextLine());
		
		System.out.print("Enter second list of letters  ");
		list2 = getListOfLetters(sc.nextLine());
		
		output.addAll(list1);
		
		for (int i = 0; i < list2.size(); i++) {
			if (!list1.contains(list2.get(i))){
				output.add(list2.get(i));
			}
		}		
		
		for (Character character : output) {
			System.out.print(character +" ");
		}
	}
	
	public static List<Character> getListOfLetters(String line){
		List<Character> list = new ArrayList<Character>();
		char[] letters = line.toCharArray();
		for (char letter : letters){
			if(letter != ' ') {
				list.add(letter);
			}
		}
		return list;
	}
}
