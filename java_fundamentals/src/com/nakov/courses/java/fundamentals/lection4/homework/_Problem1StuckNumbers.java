package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class _Problem1StuckNumbers {
	public static void main(String[] args) {
		stuckNumbers();
	}

	private static void stuckNumbers() {
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		scanner.nextLine();
		int [] numbers = new int [size];
		boolean hasfound = false;
		for (int i=0; i < numbers.length; i++) {
			numbers[i] = scanner.nextInt();
		}
		
		
		for (int i=0; i < numbers.length; i++) {
			for (int j=0; i < numbers.length; j++) {
				for (int k=0; k < numbers.length; k++) {
					for (int l=0; l < numbers.length; l++) {
						int a = numbers[i];
						int b = numbers[j];
						int c = numbers[k];
						int d = numbers[l];
						
						if (a != b && a!=c && a!=d && b!=c && b!=d && c!=d) {
							String first = a +""+ b;
							String second = c +""+ d;
							
							if (first.equals(second)) {
								System.out.printf("%d|%d==%d|%d%n",numbers[i],numbers[j],numbers[k],numbers[l]);
								hasfound = true;
							}
						}
						
					}
				}
			}
			
			if (!hasfound) {
				System.out.println("Not found");
			}
		}
	}
}
