package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Scanner;
import java.util.stream.Stream;

public class Problem13FilterArray {
	public static void main(String[] args) {
		filterArray();
	}

	private static void filterArray() {
		 Stream.of(new Scanner(System.in).nextLine().split(" ")).filter(s -> s.length() > 3).forEach(e -> System.out.print(e + " "));
	}
}
