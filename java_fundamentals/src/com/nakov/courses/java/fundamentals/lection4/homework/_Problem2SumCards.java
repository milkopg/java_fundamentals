package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.HashMap;
import java.util.Scanner;

public class _Problem2SumCards {
	public static void main(String[] args) {
		sumCards();
	}

	private static void sumCards() {
		HashMap <String, Integer> cardsMap = fillCardsMap();
		Scanner scanner = new Scanner(System.in);
		String [] cards = scanner.nextLine().split(" ");
		int sum = 0;
		String previous = null;
		String next = null;
		String current= null;
		long time1 = System.currentTimeMillis();
		
		for (int i=0; i < cards.length; i++) {
			//ignore the face
			current = cards[i].substring(0, cards[i].length()-1);
			if ((i + 1 < cards.length)) {
				next = cards[i+1].substring(0, cards[i+1].length()-1);
			} else {
				next = null;
			}
			
			if (i > 0) {
				previous = cards[i-1].substring(0, cards[i-1].length()-1);
			}
			if (cardsMap.containsKey(current)) {
				if (current.equals(previous) || current.equals(next)) {
					sum += cardsMap.get(current)*2;
				} else {
					sum += cardsMap.get(current);
				}
				
			}
		}
		System.out.println(sum);
		System.out.println(System.currentTimeMillis() - time1);
	}

	private static HashMap<String, Integer> fillCardsMap() {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		map.put("2", 2);
		map.put("3", 3);
		map.put("4", 4);
		map.put("5", 5);
		map.put("6", 6);
		map.put("7", 7);
		map.put("8", 8);
		map.put("9", 9);
		map.put("10", 10);
		map.put("J", 12);
		map.put("Q", 13);
		map.put("K", 14);
		map.put("A", 15);
		
		return map;
	}
}
