package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class Problem10ExtractAllUniqueWords {

	public static void main(String[] args) {
		extractAllUniqueWords();
	}
	
	private static void extractAllUniqueWords() {
		Scanner scanner = new Scanner(System.in);
		String[] words = scanner.nextLine().trim().toLowerCase().split("[^a-zA-Z]+");
		Arrays.sort(words);
		System.out.print(new LinkedHashSet<String>(Arrays.asList(words)).toString().replaceAll("\\[|\\]|\\,",""));
	}
}
