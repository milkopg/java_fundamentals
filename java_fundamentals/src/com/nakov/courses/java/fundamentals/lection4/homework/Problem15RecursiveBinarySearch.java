package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

public class Problem15RecursiveBinarySearch {
	public static void main(String[] args) {
		recursiveBinarySearch();
	}

	private static void recursiveBinarySearch() {
		Scanner scanner = new Scanner(System.in);
		String searchFor = scanner.next();
		scanner.nextLine();
		int[] numbers = Stream.of(scanner.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
		Arrays.sort(numbers);
		System.out.println(binarySearch(numbers, Integer.parseInt(searchFor), numbers.length/2));
	}

	private static int binarySearch(int[] numbers, int searchDigit, int startIndex) {
		int index = startIndex;
		if (startIndex == numbers.length) {
			return -1;
		}
		
		if (numbers[startIndex] == searchDigit) {
			return startIndex;
		} else if (numbers[startIndex] > searchDigit) {
			binarySearch(numbers, searchDigit, startIndex - 1);
			index--;
		} else if (numbers[startIndex] < searchDigit) {
			binarySearch(numbers, searchDigit, startIndex + 1);
			index++;
		}
		return index;
	}
}
