package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Problem8ExtractEmail {

	public static void main(String[] args) {
		extractEmail();
	}

	private static void extractEmail() {
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		
		Pattern pattern = Pattern.compile("([a-z\\d][\\w\\-\\.]*[a-z\\d]@([a-z][a-z\\-]*[a-z]\\.)+[a-z]+)");
		Matcher emailMatcher = pattern.matcher(input);
		
		while (emailMatcher.find()) {
			System.out.println(emailMatcher.group());
		}
	}
}
