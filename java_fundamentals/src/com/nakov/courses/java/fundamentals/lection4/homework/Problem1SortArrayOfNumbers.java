package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Arrays;
import java.util.Scanner;

public class Problem1SortArrayOfNumbers {

	public static void main(String[] args) {
		sortNumbers();
	}

	private static void sortNumbers() {
		Scanner scanner = new Scanner(System.in);
		int n =  scanner.nextInt();
		
		int numbers[] = new int [n];
		for (int i=0; i < numbers.length; i++) {
			numbers[i] = scanner.nextInt();
		}
		
		Arrays.sort(numbers);
		Arrays.stream(numbers).mapToObj(i -> i + " ").forEach(System.out::print);
		}

}
