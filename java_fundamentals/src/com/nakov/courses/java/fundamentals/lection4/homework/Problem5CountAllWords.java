package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Scanner;

public class Problem5CountAllWords {
	public static void main(String[] args) {
		countWords();
	}

	private static void countWords() {
		Scanner scanner = new Scanner(System.in);
		String[] words = scanner.nextLine().trim().split("[^a-zA-Z]+");
		System.out.println(words.length);
	}
}
