package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.Scanner;

public class Problem4LongestIncreasingSequence {
	public static void main(String[] args) {
		longestSequence();
	}

	private static void longestSequence() {
		Scanner scanner = new Scanner(System.in);
		ListIterator<String> iterator = Arrays.asList(scanner.nextLine().split(" ")).listIterator();
		HashMap<Integer, List<Integer>> valuesMap = new LinkedHashMap<Integer, List<Integer>>();
		List<Integer> valuesList = new ArrayList<Integer>();
		Integer index = 0;
		
		while (iterator.hasNext()) {
			//if it's first entry
			if (!iterator.hasPrevious()) {
				valuesList.add(Integer.parseInt(iterator.next()));
				valuesMap.put(index, valuesList);
			} else if (iterator.hasNext()) {
				Integer nextValue = Integer.parseInt(iterator.next());
				//check previous value with current value and put it in current list or new one
				if (valuesMap.get(index-1) != null && Collections.max(valuesMap.get(index-1)).compareTo(nextValue) < 0) {
					valuesList = valuesMap.get(index-1);
					valuesList.add(nextValue);
					valuesMap.remove(index-1);
					valuesMap.put(index, valuesList);
				} else {
					valuesList = new ArrayList<Integer>();
					valuesList.add(nextValue);
					valuesMap.put(index, valuesList);
				}
			}
			index++;
		}
		printNumbers(valuesMap);
	}
	
	private static void printNumbers(HashMap<Integer, List<Integer>> valuesMap) {
		Integer longest = 0;
		Integer index = 0;
		
		for (Entry<Integer, List<Integer>> entry : valuesMap.entrySet()) {
			System.out.println((entry.getValue().toString()).replaceAll("\\[|\\]|\\,","")); //remove [] brackets for printing
			if (longest < entry.getValue().size()) {
				longest = entry.getValue().size();
				index = entry.getKey();
			}
		}
		System.out.println("Longest: " + (valuesMap.get(index).toString().replaceAll("\\[|\\]|\\,","")));
	}
}
