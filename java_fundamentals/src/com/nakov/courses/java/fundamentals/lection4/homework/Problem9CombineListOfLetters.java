package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Problem9CombineListOfLetters {

	public static void main(String[] args) {
		combineListOfLetters();
	}

	private static void combineListOfLetters() {
		Scanner scanner = new Scanner(System.in);
		String [] lines1 = scanner.nextLine().split(" ");
		String [] lines2 = scanner.nextLine().split(" ");
		
		List<Character> list1 = new ArrayList<>();
		List<Character> list2 = new ArrayList<>();
		
		for (int i=0; i < lines1.length; i++) {
			list1.add(lines1[i].charAt(0));
		}
		for (int i=0; i < lines2.length; i++) {
			if (!list1.contains(lines2[i].charAt(0))) {
				list2.add(lines2[i].charAt(0));
			}
		}
		list1.addAll(list2);
		System.out.println(list1.toString().replaceAll("\\[|\\]|\\,",""));
	}
	
}
