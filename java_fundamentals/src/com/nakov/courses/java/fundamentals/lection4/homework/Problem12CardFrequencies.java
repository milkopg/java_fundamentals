package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Problem12CardFrequencies {

	public static void main(String[] args) {
		cardFrequences(); 
	}

	private static void cardFrequences() {
		Scanner scanner = new Scanner(System.in);
		HashMap<String, Integer> wordsMap = new LinkedHashMap<String, Integer>();
		
		Pattern pattern = Pattern.compile("(\\w+)");
        Matcher wordExtractor = pattern.matcher(scanner.nextLine());
        
        Integer count = 1;
        int totalPercentage = 0;
        while (wordExtractor.find()) {
        	String key = wordExtractor.group();
        	if (wordsMap.containsKey(key)) {
        		count = wordsMap.get(key) + 1;
        	} else {
        		count = 1;
        	}
        	wordsMap.put(key, count);
        	totalPercentage++;
        }
		
        for (Entry<String, Integer> entry : wordsMap.entrySet()) {
        	System.out.printf("%s -> %.2f%%%n" , entry.getKey(), ((double)entry.getValue() / totalPercentage) * 100);
        }
	}
}
