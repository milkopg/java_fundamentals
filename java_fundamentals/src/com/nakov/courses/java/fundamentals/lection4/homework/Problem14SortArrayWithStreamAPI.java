package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Problem14SortArrayWithStreamAPI {
	public static void main(String[] args) {
		sortArray();
	}

	private static void sortArray() {
		Scanner scanner = new Scanner(System.in);
		List<String> numbersList = Arrays.asList(scanner.nextLine().split(" "));
		String sortType = scanner.next();
		
		if ("Ascending".equals(sortType)) {
			numbersList.stream().map(s -> Integer.parseInt(s)).sorted((number1, number2) -> number1.compareTo(number2)).forEach(number -> System.out.print(number + " " ));
		} else if ("Descending".equals(sortType)) {
			numbersList.stream().map(s -> Integer.parseInt(s)).sorted((number1, number2) -> number2.compareTo(number1)).forEach(number -> System.out.print(number + " " ));
		} else {
			System.out.println("Wrong syntax!");
		}
	}
}
