package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.HashMap;
import java.util.Scanner;

public class Problem6CountSpecifiedWord {

	public static void main(String[] args) {
		countSpecifiedWord();
	}

	private static void countSpecifiedWord() {
		Scanner scanner = new Scanner(System.in);
		String[] words = scanner.nextLine().trim().toLowerCase().split("[^a-zA-Z]+");
		String word = scanner.nextLine().toLowerCase();
		HashMap<String, Integer> wordsMap = new HashMap<String, Integer>();
		int count = 1;
		
		for (int i=0; i < words.length; i++) {
			if (wordsMap.containsKey(words[i])) {
				count = wordsMap.get(words[i]) + 1;
			} else {
				count = 1;
			}
			wordsMap.put(words[i], count);
		}
		System.out.println(wordsMap.get(word) != null ? wordsMap.get(word) : 0);
	}
}
