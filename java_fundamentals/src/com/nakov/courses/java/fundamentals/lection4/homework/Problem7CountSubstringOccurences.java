package com.nakov.courses.java.fundamentals.lection4.homework;

import java.util.Scanner;

public class Problem7CountSubstringOccurences {

	public static void main(String[] args) {
		countSpecifiedWord();
	}

	private static void countSpecifiedWord() {
		Scanner scanner = new Scanner(System.in);
		String[] words = scanner.nextLine().trim().toLowerCase().split("[^a-zA-Z]+");
		String word = scanner.nextLine().toLowerCase();
		int count = 0;
		
		for (int i=0; i < words.length; i++) {
			if (words[i].contains(word)) {
				for (int j=0; j < words[i].length(); j++) {
					if(words[i].substring(j).startsWith(word)) {
						count++;
					}
				}
			}
		}
		System.out.println(count);
	}
}
