package com.nakov.courses.java.fundamentals.lection4.homework;

import java.math.BigDecimal;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _Problem3SimpleExpression {
	public static void main(String[] args) {
		simpleExpression();
	}

	private static void simpleExpression() {
		Scanner scanner = new Scanner(System.in);
		String line = scanner.nextLine().replaceAll("\\s+","");
		System.out.println(line);
		Pattern pattern = Pattern.compile("([+-]|\\b)([\\d\\.]+)");
		Matcher matcher = pattern.matcher(line);
		BigDecimal sum = new BigDecimal(0);
		
		while (matcher.find()) {
			BigDecimal number = new BigDecimal(matcher.group(2));
			String operator = matcher.group(1);
			if ("+".equals(operator) || operator.isEmpty()) {
				sum = sum.add(number);
			} else {
				sum = sum.subtract(number);
			}
		}
		System.out.println(sum);
	}
}
