package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Problem5SaveArrayListOfDoubles {
	public static void main(String[] args) {
		save();
		load();
	}

	private static void save() {
		List<Double> doublesList = new ArrayList<Double>();
		doublesList.add(1.11);
		doublesList.add(2.22);
		doublesList.add(3.33);
		
		File input = new File("res/problem5/doubles.list");
		input.getParentFile().mkdirs();
		
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(input))){
			oos.writeObject(doublesList);
		} catch (FileNotFoundException e) {
			System.err.println("File Not found");
		} catch (IOException e) {
			System.err.println("Cannot read file");
		}
	}

	@SuppressWarnings("unchecked")
	private static void load() {
		List <Double> doublesList = new ArrayList<Double>();
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("res/problem5/doubles.list")))){
			doublesList = (List<Double>) ois.readObject();
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
		} catch (IOException e) {
			System.err.println("Cannot read file");
		} catch (ClassNotFoundException e) {
			System.err.println("Class not found");
		}
		printDoublesList(doublesList);
	}

	private static void printDoublesList(List<Double> doublesList) {
		for (double d : doublesList) {
			System.out.println(d);
		}
	}
}
