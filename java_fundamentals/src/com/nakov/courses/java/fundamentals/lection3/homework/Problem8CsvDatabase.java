package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class Problem8CsvDatabase {
	
	private static final String COMMAND_SEARCH_BY_FULL_NAME = "Search-by-full-name";
	private static final String COMMAND_SEARCH_BY_ID = "Search-by-id";
	private static final String COMMAND_DELETE_BY_ID = "Delete-by-id";
	private static final String COMMAND_UPDATE_BY_ID = "Update-by-id";
	private static final String COMMAND_INSERT_STUDENT = "Insert-student";
	private static final String COMMAND_INSERT_GRADE_BY_ID = "Insert-grade-by-id";
	
	private static final String PARAMETER_GRADE_MATH = "Math";
	private static final String PARAMETER_GRADE_LITERATURE = "Literature";
	//private static final List<String> courses = Arrays.asList("Math", "Literature");
	
	private static File fileGrades = new File ("res/problem8/grades.txt");
	private static File fileStudents = new File ("res/problem8/students.txt");
	
	static ConcurrentHashMap<Integer, Problem8Student> studentsByIdMapWrapper = new ConcurrentHashMap<Integer,Problem8Student>();
	static ConcurrentHashMap<String, Problem8Student> studentsByFullNameMapWrapper = new ConcurrentHashMap<String, Problem8Student>();
	
	public static void main(String[] args) {
		//initData();
		loadData();
		readConsole();
	}

	
	private static void initData() {
		//create wrapperList to allow us to put additional elements on demands, otherwise we well get Not SupportedException, because of fixed size initialization
		List<String> grades1List = new ArrayList<String>();
		
		String [] grades1 = "Math: 2.00, 2.00, 3.50".split(" ");
		for (int i=0; i < grades1.length ; i++) {
			grades1List.add(grades1[1]);
		}
		
		List<String> grades2List = new ArrayList<String>();
		String [] grades2 = "Literature 4.00 5.25".split(" ");
		for (int i=0; i < grades1.length ; i++) {
			grades2List.add(grades2[1]);
		}
		
		
		Problem8Grades gradesObject = new Problem8Grades(5, grades1List, grades2List);
		Problem8Student studentObject = new Problem8Student(5, "Georgi", "Ivanov",14, "Novi Pazar", gradesObject);
		
		saveStudent(studentObject);
		saveGrades(gradesObject);
		
		studentsByIdMapWrapper.put(studentObject.getStudentId(), studentObject);
		studentsByFullNameMapWrapper.put(studentObject.getFirstName() + " " + studentObject.getLastName(), studentObject);
	}
	
	private static void loadData() {
		try (AppendableObjectInputStream oisGrades = new AppendableObjectInputStream(new FileInputStream(fileGrades));
				AppendableObjectInputStream oisStudents = new AppendableObjectInputStream(new FileInputStream(fileStudents))){
			Problem8Student student;
			Problem8Grades grade;
			
			while (true) {
				try {
					student = (Problem8Student) oisStudents.readObject();
				} catch (Exception e) {
					break;
				}
				
				studentsByIdMapWrapper.put(student.getStudentId(), student);
				studentsByFullNameMapWrapper.put(student.getFirstName() + " " + student.getLastName(), student);
			}
			while (true) {
				try {
					grade = (Problem8Grades) oisGrades.readObject();
				} catch (Exception e) {
					break;
				}
				if (studentsByIdMapWrapper.contains(grade.getStudentId())) {
					studentsByIdMapWrapper.get(grade.getStudentId()).setGrades(grade);
				}
				
			}
			//oisGrades.close();
			//oisStudents.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private static void saveStudent(Problem8Student student) {
		try (
				AppendableObjectOutputStream ousStudents = new AppendableObjectOutputStream(new FileOutputStream(fileStudents, true))){
				ousStudents.writeObject(student);
			} catch (FileNotFoundException e) {
				System.err.println("File not found");
			} catch (IOException e) {
				System.err.println("Cannot read file");
			}
	}
	
	private static void saveGrades(Problem8Grades grades) {
		try (
			AppendableObjectOutputStream ousGrades = new AppendableObjectOutputStream(new FileOutputStream(fileGrades, true))){
			ousGrades.writeObject(grades);
			} catch (FileNotFoundException e) {
				System.err.println("File not found");
			} catch (IOException e) {
				System.err.println("Cannot read file");
			}
	}

	private static void readConsole() {
		Scanner scanner = new Scanner(System.in);
		String line = scanner.nextLine();
		scanner.close();
		
		parseCommands(line);
		}

	private static void parseCommands(String line) {
		
		if (line.startsWith(COMMAND_SEARCH_BY_FULL_NAME)) {
			performSerchByFillName(line);
		} else if (line.startsWith(COMMAND_SEARCH_BY_ID)) {
			performSerchById(line);
		} else if (line.startsWith(COMMAND_DELETE_BY_ID)) {
			performDeleteById(line);
		} else if (line.startsWith(COMMAND_UPDATE_BY_ID)) {
			performUpdateById(line);
		} else if (line.startsWith(COMMAND_INSERT_STUDENT)) {
			performInsertStudentById(line);
		} else if (line.startsWith(COMMAND_INSERT_GRADE_BY_ID)) {
			performInsertGradeById(line);
		} 
	}


	private static void performSerchByFillName(String line) {
		String [] lines = line.split(" ") ;
		String lastName = "";
		for (int i=0; i < lines.length ; i++) {
			if (i==0) continue;
			lastName = lastName + lines[i] + " ";
		}
		Problem8Student result = studentsByFullNameMapWrapper.get(lastName.trim());
		printStudent(result);
 	}

	private static void performSerchById(String line) {
		String [] lines = line.split(" ");
		String studentIdStr = "";
		int studentId = 0;
		for (int i = 0; i < lines.length; i++) {
			if ((i==0) || (i > 1)) continue;
			studentIdStr = lines[i];
		}
		try {
			studentId = Integer.parseInt(studentIdStr);
		} catch (NumberFormatException nfe) {
			System.out.println("No valid student id:");
			return;
		}
		
		Problem8Student result = studentsByIdMapWrapper.get(studentId);
		printStudent(result);
		
	}

	private static void performDeleteById(String line) {
		// TODO Auto-generated method stub
		
	}

	private static void performUpdateById(String line) {
		// TODO Auto-generated method stub
		
	}

	private static void performInsertStudentById(String line) {
		String lines [] = line.split(" ");
		if (lines == null || lines.length != 5) error("Wrong syntax!");
		
		int newId = getMaxStudentId() + 1;
		Problem8Student student = new Problem8Student(newId, lines[1], lines[2], Integer.parseInt(lines[3]), lines[4], null);
		saveStudent(student);
		studentsByIdMapWrapper.put(newId, student);
		printStudent(student);
	}
	
	private static void performInsertGradeById(String line) {
		String lines [] = line.split(" ");
		if (lines == null || lines.length != 4) error("Wrong syntax!");;
		int id = Integer.parseInt(lines[1]);
		
		if (lines[2] != null) {
			Problem8Student student = studentsByIdMapWrapper.get(id);
			if (student == null) return;
			if (lines[2].startsWith(PARAMETER_GRADE_MATH)) { 
				//student.getGrades().getCourse1Grades().add(lines[3]);
				List<String> grade1 = new ArrayList<String>();
				grade1.addAll(student.getGrades().getCourse1Grades());
				grade1.add(lines[3]);
				student.getGrades().setCourse1Grades(grade1);
//				student.getGrades().getCourse1Grades().add(lines[3]);
//				student.getGrades().setCourse1Grades(new ArrayList<String>);
				
			} else if (lines[2].startsWith(PARAMETER_GRADE_LITERATURE)) {
				student.getGrades().getCourse2Grades().add(lines[3]);
			}
			saveGrades(student.getGrades());
		}
	}

	
	private static int getMaxStudentId() {
		List<Integer> studentIdList = new ArrayList<Integer>();
				
		for(Entry<Integer, Problem8Student> student: studentsByIdMapWrapper.entrySet()) {
			studentIdList.add(student.getKey());
		}
		return studentIdList.isEmpty() ? 0 : Collections.max(studentIdList);
	}

	private static void printStudent(Problem8Student student) {
		if (student != null) {
			if (student.getGrades() != null) {
				System.out.println("" + student + student.getGrades());
			} else {
				System.out.println(student);
			}
			
			
		} else {
			System.out.println("Student does not exists");
		}
	}
	
	private static void error (String message) {
		System.err.println(message);
		System.exit(0);
	}
	
	private static class AppendableObjectOutputStream extends ObjectOutputStream {
        public AppendableObjectOutputStream(OutputStream out) throws IOException {
          super(out);
        }

        @Override
        protected void writeStreamHeader() throws IOException {}
  }
	
	private static class AppendableObjectInputStream extends ObjectInputStream {
        public AppendableObjectInputStream(InputStream in) throws IOException {
          super(in);
        }

        @Override
        protected void readStreamHeader() throws IOException,
        		StreamCorruptedException {
        }
	}
}
