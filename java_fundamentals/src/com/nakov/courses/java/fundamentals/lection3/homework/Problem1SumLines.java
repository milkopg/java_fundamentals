package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Problem1SumLines {

	public static void main(String[] args) {
		sumLines();
	}

	private static void sumLines() {
		try (BufferedReader br = new BufferedReader(new FileReader(new File("res/problem1/lines.txt")))){
			String line;
			long sum = 0;
			while ((line = br.readLine()) != null) {
				for (int i=0; i < line.length(); i++) {
					sum += line.charAt(i);
				}
				System.out.println(sum);
				sum = 0;
			}
		} catch (FileNotFoundException e) {
			System.err.println("File not found!");
		} catch (IOException e1) {
			System.err.println("Cannot read file!");
		}
	}
}
