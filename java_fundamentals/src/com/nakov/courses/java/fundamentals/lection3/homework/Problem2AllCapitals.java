package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Problem2AllCapitals {

	public static void main(String[] args) {
		allCapitals();
	}

	private static void allCapitals() {
		String filePath = "res/problem2/lines.txt";
		File input = new File(filePath);
		File output = new File(filePath + ".tmp");
		output.getParentFile().mkdirs();
		
		try (	
				BufferedReader br = new BufferedReader(new FileReader(input));
				BufferedWriter bw = new BufferedWriter(new FileWriter(output));
			){
			String line;
			while ((line = br.readLine()) != null) {
				line = line.toUpperCase();
				bw.write(line);
				bw.newLine();
			}
		} catch (FileNotFoundException e) {
			System.err.println("File not found!");
		} catch (IOException e1) {
			System.err.println("Cannot read file!");
		}
		
		if(input.delete()) {
			output.renameTo(input);
		}
	}
}
