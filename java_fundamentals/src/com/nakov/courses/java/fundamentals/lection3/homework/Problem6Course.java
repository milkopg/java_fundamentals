package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.Serializable;

public class Problem6Course implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5335956126388095868L;
	private String name;
	private int numberOfStudents;
	
	public Problem6Course(String name, int numberOfStudents) {
		this.name = name;
		this.numberOfStudents = numberOfStudents;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumberOfStudents() {
		return numberOfStudents;
	}
	public void setNumberOfStudents(int numberOfStudents) {
		this.numberOfStudents = numberOfStudents;
	}
	
	@Override
	public String toString() {
		return "Course name: " + name + ", number of students: " +  numberOfStudents;
	}
}
