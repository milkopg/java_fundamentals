package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Problem6SaveCustomObjectInFile {
	private static Problem6Course courses [] ;
	public static void main(String[] args) {
		courses = new Problem6Course[3];
		courses[0] = new Problem6Course("Java fundamentals", 1200);
		courses[1] = new Problem6Course("Algorithms", 1100);
		courses[2] = new Problem6Course("Java BackEnd", 1000);
		
		save();
		load();
	}
	
	private static void save() {
		File input = new File("res/problem6/course.save");
		input.getParentFile().mkdirs();
		
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(input))){
			oos.writeObject(courses);
		} catch (FileNotFoundException e) {
			System.err.println("File Not found");
		} catch (IOException e) {
			System.err.println("Cannot read file");
		}
	}
	
	private static void load() {
		Problem6Course coursesDeserialized [] = null;
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("res/problem6/course.save")))){
			coursesDeserialized =  (Problem6Course[]) ois.readObject();
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
		} catch (IOException e) {
			System.err.println("Cannot read file");
		} catch (ClassNotFoundException e) {
			System.err.println("Class not found");
		}
		
		//print records
		for (Problem6Course course : coursesDeserialized) {
			System.out.println(course);
		}
		
	}
}
