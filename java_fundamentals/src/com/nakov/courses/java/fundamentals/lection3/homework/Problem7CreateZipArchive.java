package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Problem7CreateZipArchive {

	public static void main(String[] args) {
		createZipArchive();
	}

	private static void createZipArchive() {
		File [] files = new File [3] ;
		files[0] = new File("res/problem3/words.txt");
		files[1] = new File("res/problem3/count-chars.txt");
		files[2] = new File("res/problem1/lines.txt");
		File fileZip = new File ("res/problem7/text-files.zip");
		fileZip.getParentFile().mkdirs();
		
		byte [] buffer = new byte [1024];
		File file = null;
			
			try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(fileZip))) {
				for (int i=0 ; i < files.length ; i++) {
					file = files[i];
				
					BufferedInputStream  bis = new BufferedInputStream( new FileInputStream(file));
					
					zos.putNextEntry(new ZipEntry(file.getName()));
					int length;
					while ((length = bis.read(buffer)) != -1) {
						zos.write(buffer, 0, length);
					}
					zos.closeEntry();
					bis.close();
				}
			} catch (FileNotFoundException e) {
				System.err.println("File not found: " );
			} catch (IOException e1) {
				System.err.println("Cannot read file: ");
			}
		}
	}

