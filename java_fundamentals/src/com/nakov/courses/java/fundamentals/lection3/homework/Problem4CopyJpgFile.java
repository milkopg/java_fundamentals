package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Problem4CopyJpgFile {

	public static void main(String[] args) {
		copyJpgFile();
	}

	private static void copyJpgFile() {
		File input = new File("res/problem4/birthday.jpg");
		File output = new File("res/problem4/my-copied-picture.jpg");
		output.getParentFile().mkdirs();
		
		try (
				
				FileInputStream fis = new FileInputStream(input);
				FileOutputStream fos = new FileOutputStream(output)
				) {
			byte [] buffers = new byte[(int) input.length()];
			fis.read(buffers);
			fos.write(buffers);
			
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
		} catch (IOException e) {
			System.err.println("Cannot read file");
		}
	}
}
