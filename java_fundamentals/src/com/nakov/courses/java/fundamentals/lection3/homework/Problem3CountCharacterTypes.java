package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Problem3CountCharacterTypes {

	static final char [] vowels = {'a', 'e', 'i', 'o', 'u' };
	static final char [] punctionations = {'!', ',', '.', '?'};
	
	public static void main(String[] args) {
		countCharacterTypes();
	}

	private static void countCharacterTypes() {
		int countVowels = 0;
		int countConsonants = 0;
		int countPunctionations = 0;
		try (	
				BufferedReader br = new BufferedReader(new FileReader("res/problem3/words.txt"));
				BufferedWriter bw = new BufferedWriter(new FileWriter("res/problem3/count-chars.txt"))
			){
			int readChar;
			while ((readChar = br.read()) != -1 ) {
				if(isVowel(readChar)) {
					countVowels++;
				} else {
					if(isPunctionation(readChar)) {
						countPunctionations++;
					} else if(readChar != ' ' ) {
						countConsonants++;
					}
				}
			}
			bw.write("Vowels: " + countVowels + "\nConsolidants: " + countConsonants + "\nPunctuation: " + countPunctionations);
		} catch (IOException e1) {
			System.err.println("Cannot read file!");
		}
	}

	private static boolean isPunctionation(int readChar) {
		for (int i=0; i < punctionations.length; i++) {
			if (punctionations[i] == readChar) {
				return true;
			}
		}
		return false;
	}

	private static boolean isVowel(int readChar) {
		for (int i=0; i < vowels.length; i++) {
			if (vowels[i] == readChar) {
				return true;
			}
		}
		return false;
	}
}
