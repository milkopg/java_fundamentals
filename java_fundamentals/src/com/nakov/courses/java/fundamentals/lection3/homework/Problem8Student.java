package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.Serializable;

public class Problem8Student implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4254442058281018089L;
	private int studentId;
	private String firstName;
	private String lastName;
	private int age;
	private String homeTown;
	private Problem8Grades grades;
	
	public Problem8Student(int studentId, String firstName, String lastName, int age, String homeTown, Problem8Grades grades) {
		this.studentId = studentId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.homeTown = homeTown;
		this.grades = grades;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getHomeTown() {
		return homeTown;
	}

	public void setHomeTown(String homeTown) {
		this.homeTown = homeTown;
	}
	
	public Problem8Grades getGrades() {
		return grades;
	}

	public void setGrades(Problem8Grades grades) {
		this.grades = grades;
	}

	@Override
	public String toString() {
		return getFirstName() + " " + getLastName() + " (age: " + getAge() + ", town: " + getHomeTown() + ", id: " + getStudentId() + ")\n"; 
	}
}
