package com.nakov.courses.java.fundamentals.lection3.homework;

import java.io.Serializable;
import java.util.List;

public class Problem8Grades implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3268694313766254532L;
	private int studentId;
	private List<String> course1Grades;
	private List<String> course2Grades;
	
	public Problem8Grades(int studentId, List<String> course1Grades, List<String> course2Grades) {
		this.studentId = studentId;
		this.course1Grades = course1Grades;
		this.course2Grades = course2Grades;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public List<String> getCourse1Grades() {
		return course1Grades;
	}
	
	public String getCourse1GradesToString() {
		String course1GradesStr = "";
		for (int i=0; i < course1Grades.size() ; i++) {
			course1GradesStr += course1Grades.get(i)+ " ";
		}
		return course1GradesStr;
	}


	public void setCourse1Grades(List<String> course1Grades) {
		this.course1Grades = course1Grades;
	}

	public List<String> getCourse2Grades() {
		return course2Grades;
	}
	
	
	public void setCourse2Grades(List<String> course2Grades) {
		this.course2Grades = course2Grades;
	}
	
	public String getCourse2GradesToString() {
		String course2GradesStr = "";
		for (int i=0; i < course2Grades.size() ; i++) {
			course2GradesStr += course2Grades.get(i) + " ";
		}
		return course2GradesStr;
	}
	
	@Override
	public String toString() {
		return "# " + getCourse1GradesToString() + "\n# " + getCourse2GradesToString();
	}
}
