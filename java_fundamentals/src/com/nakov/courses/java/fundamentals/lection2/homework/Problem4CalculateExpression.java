package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.Locale;
import java.util.Scanner;

public class Problem4CalculateExpression {

	public static void main(String[] args) {
		calculateExpressin();
	}

	private static void calculateExpressin() {
		Scanner scanner = new Scanner(System.in);
		Locale locale = Locale.US;
		scanner.useLocale(locale);
		float a = scanner.nextFloat();
		float b = scanner.nextFloat();
		float c = scanner.nextFloat();
		scanner.close();
		
		float f1 = calculateF1(a, b, c);
		float f2 = calculateF2(a, b, c);
		
		float average = Math.abs(average(a, b, c) - average(f1, f2));
		
		System.out.println(String.format(locale, "F1 result: %.2f; F2 result: %.2f ; Diff: %.2f", f1, f2, average));
	}

	private static float calculateF1(float a, float b, float c) {
		return (float) Math.pow((Math.pow(a, 2) + Math.pow(b, 2))/(Math.pow(a, 2) - Math.pow(b, 2)), (a + b + c) / Math.sqrt(c));
	}

	private static float calculateF2(float a, float b, float c) {
		return (float) Math.pow(Math.pow(a, 2) + Math.pow(b, 2) - Math.pow(c, 3), (a-b));
	}
	
	private static float average(float f1, float f2) {
		return  (f1 + f2) / 2;
	}

	private static float average (float a, float b, float c) {
		return (a + b + c) / 3;
	}
}
