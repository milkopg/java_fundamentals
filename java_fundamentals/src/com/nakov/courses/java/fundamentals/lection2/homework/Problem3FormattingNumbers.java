package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.Scanner;

public class Problem3FormattingNumbers {

	public static void main(String[] args) {
		formatNumbers();
	}

	@SuppressWarnings("resource")
	private static void formatNumbers() {
		Scanner scanner = new Scanner(System.in);
		
		int a = scanner.nextInt();
		if (a < 0 || a > 500) {
			System.out.println("a must be bewteen 0 and 500. Exit!");
			return;	
		}
		float b = scanner.nextFloat();
		float c = scanner.nextFloat();
		
		scanner.close();
		System.out.println(String.format("|%-10s|%10s|%10.2f|%-10.3f|", Integer.toHexString(a).toUpperCase() , String.format("%10s", Integer.toBinaryString(a)).replace(' ', '0'), b ,  c));
	}

}
