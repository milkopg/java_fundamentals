package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Problem11StartAndEndWithCapitalLetter {
	public static void main(String[] args) {
		exctractCapitalLetter();
	}

	private static void exctractCapitalLetter() {
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		scanner.close();
		
		Pattern pattern = Pattern.compile("\\b[A-Z][a-zA-Z]*[A-Z]\\b|\\b[A-Z]\\b"); 
		Matcher matcher = pattern.matcher(input); 
		while (matcher.find()) {
			System.out.print(matcher.group() + " ");
		}
	}
}
