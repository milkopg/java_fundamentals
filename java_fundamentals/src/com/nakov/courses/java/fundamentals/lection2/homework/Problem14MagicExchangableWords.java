package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.HashMap;
import java.util.Scanner;

public class Problem14MagicExchangableWords {
	public static void main(String[] args) {
		readValues();
	}

	private static void readValues() {
		Scanner scanner = new Scanner(System.in);
		String input1 = scanner.next();
		String input2 = scanner.next();
		
		scanner.close();
		System.out.println(exchangableStrings(input1, input2));
	}

	private static boolean exchangableStrings(String input1, String input2) {
		HashMap <Character, Character> valuesMap = new HashMap<Character, Character>();
		boolean exchangable = true;
		
		if (((input1 == null) || (input2 == null)) || (input1.length() != input2.length())) {
			System.out.println("Mismatched length. Exit");
			return !exchangable;
		}
		
		for (int i=0; i < input1.length(); i++) {
			if (!valuesMap.containsKey(input1.charAt(i))) {
				valuesMap.put(input1.charAt(i), input2.charAt(i));
			} else if (!valuesMap.get(input1.charAt(i)).equals(input2.charAt(i))){
				exchangable = false;
			}
		}
		return exchangable;
	}
}
