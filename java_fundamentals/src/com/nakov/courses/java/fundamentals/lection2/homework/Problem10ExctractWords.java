package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Problem10ExctractWords {
	public static void main(String[] args) {
		exctractWords();
	}

	private static void exctractWords() {
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		scanner.close();
		
		Pattern pattern = Pattern.compile("[a-zA-Z]+");
		Matcher matcher = pattern.matcher(input); 
		while (matcher.find()) {
			System.out.print(matcher.group() + " ");
		}
	}
}
