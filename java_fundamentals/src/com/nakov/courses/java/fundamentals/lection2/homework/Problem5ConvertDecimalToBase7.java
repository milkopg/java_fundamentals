package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.Scanner;

public class Problem5ConvertDecimalToBase7 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = scanner.hasNextInt() ? scanner.nextInt() : -1;
		scanner.close();
		
		if (number > 0) {
			System.out.println(Integer.toString(number, 7));
		}
	}
}
