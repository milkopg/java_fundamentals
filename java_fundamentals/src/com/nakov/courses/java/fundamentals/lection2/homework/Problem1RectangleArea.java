package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.Scanner;

public class Problem1RectangleArea {
	public static void main(String[] args) {
		getValuesFromConsole();
	}

	private static void getValuesFromConsole() {
		Scanner scanner = new Scanner(System.in);
		int sideA = scanner.nextInt();
		int sideB = scanner.nextInt();
		scanner.close();
		System.out.println("Area of rectangle is: " + sideA * sideB);
	}
}
