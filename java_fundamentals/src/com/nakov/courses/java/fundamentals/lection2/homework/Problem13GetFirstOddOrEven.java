package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Problem13GetFirstOddOrEven {
	private static enum TypeNumber{
		odd, even
	};
	
	public static void main(String[] args) {
		readValues();
	}

	private static void readValues() {
		Scanner scanner = new Scanner(System.in);
		
		String [] inputArray = scanner.nextLine().split(" ");
		List<Integer> integerList = fillIntegerlist(inputArray);

		String [] commandsArray = scanner.nextLine().split(" ");
		scanner.close();
		
		parseCommands(Arrays.asList(commandsArray), integerList);
	}

	
	private static List<Integer> fillIntegerlist(String[] inputArray) {
		List<Integer> integerList = new ArrayList<Integer>();
		for (String input : inputArray) {
			try {
				integerList.add(Integer.parseInt(input));
			} catch (NumberFormatException nfe){
				System.out.println("Wrong integer input! Exit! ");
			}
		}
		return integerList;
	}
	
	private static void parseCommands(List<String> commandsList, List<Integer> integerList) {
		int numberDigits = 0;
		if (!commandsList.contains("Get"))  {
			errorMessage();
		} else {
			try {
				numberDigits = Integer.parseInt(commandsList.get(1));
			} catch (NumberFormatException nfe){
				errorMessage();
			}
		}
		
		boolean oddOrEvenFound = false;
		for (TypeNumber typeNumber : TypeNumber.values()) {
			if (typeNumber.toString().equals(commandsList.get(2))) {
				findOddOrEven(typeNumber, integerList, numberDigits);
				oddOrEvenFound = true;
			}
		}
		if (!oddOrEvenFound) {
			errorMessage();
		}
	}
	
	private static void errorMessage() {
		System.out.println("Wrong syntaxt! Exit");
		return;
	}

	private static void findOddOrEven(TypeNumber typeNumber,	List<Integer> integerList, int numberOfDigits) {
		int counter = 0;
		for (int i=0; i < integerList.size(); i++) {
			if (typeNumber.equals(TypeNumber.even) && integerList.get(i) % 2 == 0) {
				System.out.print(integerList.get(i)  +  " ");
				counter++;
			} else if (typeNumber.equals(TypeNumber.odd) && integerList.get(i) % 2 == 1) {
				System.out.print(integerList.get(i)   +  " ");
				counter++;
			}
			
			if (counter >= numberOfDigits) {
				break;
			}
		}
	}
}
