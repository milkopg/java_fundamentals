package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Problem7RandomizeNumbersFromRange {
	public static void main(String[] args) {
		randomValues();
	}

	private static void randomValues() {
		Random random = new Random();
		Scanner scanner = new Scanner(System.in);
		ArrayList<Integer> listNumbers;
		int n = scanner.hasNextInt() ? scanner.nextInt() : -1;
		int m = scanner.hasNextInt() ? scanner.nextInt() : -1;
		scanner.close();
		
		if ((n > 0) && (m > 0) && (n > m)) {
			listNumbers = new ArrayList<Integer>();
			while (true) {
				int randomNumber = random.nextInt(n - m + 1) + m;
				if (!listNumbers.contains(randomNumber)) {
					listNumbers.add(randomNumber);
					System.out.print(randomNumber + " ");
				}
				
				if (listNumbers.size() == n - m ) {
					break;
				}
			}
		}
	}
}
