package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.Scanner;

public class Problem12CharacterMultiplier {
	public static void main(String[] args) {
		mupliplier();
	}

	private static void mupliplier() {
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		scanner.close();
		
		String [] inputList = input.split(" ");
		
		int maxLength = getMaxLength(inputList); 
		
		long sum = 0L;
		for (int i=0; i < inputList.length - 1; i++) {
			for (int j=0; j < maxLength; j++) {
				sum += multiply (getCharFromString(inputList[i], j), getCharFromString(inputList[i+1], j));
			}
		}
		System.out.println(sum);
	}

	private static int getCharFromString(String input, int charIndex) {
		return charIndex >= input.length() ? ' ': input.charAt(charIndex);
	}

	private static int getMaxLength(String [] inputList) {
		int maxLength = 0;
		for (String input : inputList) {
			if (input.length() > maxLength) {
				maxLength = input.length();
			}
		}
		return maxLength;
	}

	private static long multiply(int firstChar, int secondChar) {
		if (firstChar == ' ') return secondChar;
		if (secondChar == ' ') return firstChar;
		
		return firstChar * secondChar; 
	}
}
