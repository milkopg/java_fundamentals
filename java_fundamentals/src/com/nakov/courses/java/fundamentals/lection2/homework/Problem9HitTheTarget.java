package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.Scanner;

public class Problem9HitTheTarget {
	public static void main(String[] args) {
		readValuesFromConsole();
	}

	private static void readValuesFromConsole() {
		Scanner scanner = new Scanner(System.in);
		int number = scanner.hasNextInt() ? scanner.nextInt() : -1;
		scanner.close();
		
		if (number < 0) return;
		
		for (int i = 1 ; i <= 20; i++) {
			for (int j=1; j <= 20; j++) {
				if (i + j == number) {
					System.out.print(String.format("%s + %s = %s\n", i, j, number));
				} else if (i-j == number) {
					System.out.print(String.format("%s - %s = %s\n", i, j, number));
				}
			}
		}
	}
}
