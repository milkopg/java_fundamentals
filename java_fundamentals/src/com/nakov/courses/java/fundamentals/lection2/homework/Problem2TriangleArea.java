package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.Scanner;

public class Problem2TriangleArea {

	public static void main(String[] args) {
		calculateArea();
	}

	private static void calculateArea() {
		Scanner scanner = new Scanner(System.in);
		int sideAx = scanner.nextInt();
		int sideAy = scanner.nextInt();
		int sideBx = scanner.nextInt();
		int sideBy = scanner.nextInt();
		int sideCx = scanner.nextInt();
		int sideCy = scanner.nextInt();
		
		scanner.close();
		int area = Math.abs((sideAx*(sideBy - sideCy) + sideBx*(sideCy-sideAy) + sideCx*(sideAy-sideBy))/2);
		System.out.println(area);
	}
}
