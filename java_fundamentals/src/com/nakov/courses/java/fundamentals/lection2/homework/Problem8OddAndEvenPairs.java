package com.nakov.courses.java.fundamentals.lection2.homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Problem8OddAndEvenPairs {
	public static void main(String[] args) {
		readValues();
	}

	private static void readValues() {
		Scanner scanner = new Scanner(System.in);
		String inputLine = scanner.nextLine();
		String [] input = inputLine.split(" ");
		List<Integer> numbersList = new ArrayList<Integer>();
		scanner.close();
		
		if ((input != null && input.length % 2 == 0)) {
			numbersList = fillNumbersIntoList(input);
			parseAndCompareNumbers(numbersList);
		} else {
			System.out.println("Invalid length");
		}
	}

	private static List<Integer> fillNumbersIntoList(String[] input) {
		List <Integer> numbersList = new ArrayList<Integer>();
		for (int i = 0; i < input.length ; i++) {
			
			try {
				numbersList.add(Integer.parseInt(input[i]));
			} catch (NumberFormatException nfe) {
				System.out.println("Wrong input");
				break;
			}
		}
		return numbersList;
	}

	private static void parseAndCompareNumbers(List<Integer> listNumbers) {
		for (int i = 0; i < listNumbers.size() ; i++) {
			checkEvenOrOdd(listNumbers, i);
		}
	}

	private static void checkEvenOrOdd(List<Integer> numbersList, int index) {
		// check only even numbers to avoid Out of bounds exception
		if (index % 2 == 0) {
			if ((numbersList.get(index) % 2 == 0) && (numbersList.get(index+1) % 2 == 0)) {
				System.out.println(String.format("%d, %d -> both are even", numbersList.get(index), numbersList.get(index+1)));
			} else if ((numbersList.get(index) % 2 == 1) && (numbersList.get(index+1) % 2 == 1)) {
				System.out.println(String.format("%d, %d -> both are odd", numbersList.get(index), numbersList.get(index+1)));
			} else {
				System.out.println(String.format("%d, %d -> different", numbersList.get(index), numbersList.get(index+1)));
			}
		}
	}
}
