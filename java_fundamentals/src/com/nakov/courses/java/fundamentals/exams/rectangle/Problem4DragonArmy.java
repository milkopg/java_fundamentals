package com.nakov.courses.java.fundamentals.exams.rectangle;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class Problem4DragonArmy {
	
	public static void main(String[] args) {
		readValues();
	}

	private static void readValues() {
		Scanner scanner = new Scanner(System.in);
		int rows = Integer.parseInt(scanner.nextLine());
		int cols = 5;
		String lines [][] = new String[rows][cols];
		
		for (int i=0; i < rows; i++) {
			String line = scanner.nextLine();
			lines[i] = line.split(" ");
		}
		
		fillData(lines);
	}

	private static void fillData(String[][] lines) {
		Map<String, Dragon> dragonTypeMap = new LinkedHashMap<String, Dragon>();
		Map<String, Dragon>  dragonNameMap = new TreeMap<>();
		
		
		for (int i=0; i < lines.length; i++) {
			String line[] = lines[i];
			String name = line[1];
			String type = line[0];
			Double damage = line[2].equals("null") ? new Double (45): new Double(line[2]);
			Double health = line[3].equals("null") ? new Double(250) : new Double(line[3]);
			Double armor = line[4].equals("null") ? new Double(10) : new Double(line[4]);
			Dragon dragon = new Dragon(name, type, damage , health, armor);
			
			if (dragonTypeMap.containsKey(type)) {
				dragonTypeMap.put(type, new Dragon(name, type,   damage, health,  armor));
			} else {
				dragonTypeMap.put(type, dragon);
			}
			
			if (dragonNameMap.containsKey(name)) {
				dragonNameMap.put(name, dragon);
			}
			
		}
		print(dragonTypeMap, dragonNameMap);
	}

	private static void print(Map<String, Dragon> dragonTypeMap, Map<String, Dragon> dragonNameMap) {
		for (Entry <String, Dragon> entryType : dragonTypeMap.entrySet()) {
			//System.out.printf("%s::(%.2f/%.2f/%.2f)\n", entryType.getValue().getType(), entryType.getValue().getDamage(), entryType.getValue().getHealth(), entryType.getValue().getArmor());
			System.out.printf("%s::(%.2f/%.2f/%.2f)\n",  entryType.getValue().getType(), calculateDamage(entryType.getValue().getType(), dragonNameMap),  calculateHealth(entryType.getValue().getType(), dragonNameMap) , calculateArmor(entryType.getValue().getType(), dragonNameMap));
			for (Entry <String, Dragon> entryName : dragonNameMap.entrySet()) {
				if (entryType.getKey().equals(entryName.getValue().getType()) /* && entryType.getValue().getName().equals(entryName.getKey())*/) {
					System.out.printf("-%s -> damage: %s, health: %s, armor: %s\n", entryName.getKey(), formatDoubleToString("#######", entryName.getValue().getDamage()) , formatDoubleToString("#######", entryName.getValue().getHealth()), formatDoubleToString("#######", entryName.getValue().getArmor()));
				}
			}
			
		}
	}
	
	public static String formatDoubleToString(String pattern, Double number) {
		DecimalFormat formatter = new DecimalFormat(pattern);
		return formatter.format(number);
	}
	
	public static Double calculateDamage(String type,  Map<String, Dragon> dragonNameMap) {
		double stats=0.0f;
		int divider = 0;
		for (Entry<String, Dragon> entryName : dragonNameMap.entrySet()) {
			if (entryName.getValue().getType().equals(type)) {
				stats+= entryName.getValue().getDamage();
				divider++;
			}
			
		}
		return stats/divider;
	}
	
	public static Double calculateHealth(String type,  Map<String, Dragon> dragonNameMap) {
		double stats=0.0f;
		int divider = 0;
		for (Entry<String, Dragon> entryName : dragonNameMap.entrySet()) {
			if (entryName.getValue().getType().equals(type)) {
				stats+= entryName.getValue().getHealth();
				divider++;
			}
			
		}
		return stats/divider;
	}
	
	public static Double calculateArmor(String type,  Map<String, Dragon> dragonNameMap) {
		double stats=0.0f;
		int divider = 0;
		for (Entry<String, Dragon> entryName : dragonNameMap.entrySet()) {
			if (entryName.getValue().getType().equals(type)) {
				stats+= entryName.getValue().getArmor();
				divider++;
			}
			
		}
		return stats/divider;
	}
}

class Dragon  {
	private String name;
	private String type;
	private Double damage;
	private Double health;
	private Double armor;
	
	public Dragon(String name, String type, Double damage, Double health, Double armor) {
		this.name = name;
		this.type = type;
		this.damage = damage;
		this.health = health;
		this.armor = armor;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public Double getDamage() {
		return damage;
	}

	public Double getHealth() {
		return health;
	}

	public Double getArmor() {
		return armor;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Dragon) {
			if (((Dragon) obj).getName().equals(getName()) /*&& ((Dragon)(obj)).getType().equals(getType())*/) {
				return true;
			}
		}
		return false;
	}
}