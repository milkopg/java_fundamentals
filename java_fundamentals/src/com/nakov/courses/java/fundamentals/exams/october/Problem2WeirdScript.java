package com.nakov.courses.java.fundamentals.exams.october;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Problem2WeirdScript {
	public static void main(String[] args) {
		 Scanner scn = new Scanner(System.in);

//	        int keyNumber = Integer.parseInt(scn.nextLine());
//	        keyNumber = keyNumber - 1;
//	        int charCode = keyNumber % 26;
//
//	        char keyLetter = (keyNumber / 26) % 2 == 0
//	                ? (char)('a' + charCode)
//	                : Character.toUpperCase((char)('a' + charCode));
//
//	        String key = "" + keyLetter + keyLetter;
//	        StringBuilder sb = new StringBuilder();
//
//	        String line = scn.nextLine();
//	        while (!line.equals("End")) {
//	            sb.append(line);
//	            line = scn.nextLine();
//	        }
//
//	        String message = sb.toString();
//	        String pattern = key + "(.*?)" + key;
//	        Pattern pat = Pattern.compile(pattern);
//	        Matcher match = pat.matcher(message);
//
//	        while (match.find()) {
//	            if (match.group(1).length() > 0) {
//	                System.out.println(match.group(1));
//	            }
//	        }
		readValues();
	}

	private static void readValues() {
		Scanner scanner = new Scanner(System.in);
		String line = scanner.nextLine();
		int remainder = Integer.parseInt(line) % 52;
		char singleKey = (char) translateMagicKeyToLetter(remainder);
		//System.out.println(remainder);
		//System.out.println(singleKey);
		String key = singleKey + singleKey + "(.*?)" + singleKey+singleKey; 
		StringBuilder output = new StringBuilder();
		Pattern pattern = Pattern.compile(key);
		Matcher matcher = null;
		while (!"End".equals(line)) {
			output.append(line);
			output.append("\n");
//			if (line.contains(key)) {
//				System.out.println(line.substring(line.indexOf(key) + 2, line.indexOf(key, line.indexOf(key) + 2)));
//				output.append(line.substring(line.indexOf(key) + 2, line.indexOf(key, line.indexOf(key) + 2)));
//			}
			
			line = scanner.nextLine();
		}
		
		String translatedOutput = translateInput(output.toString(), "" + singleKey+singleKey); 
		System.out.println(output);
		matcher = pattern.matcher(output);
		
		while (matcher.find()) {
			if (matcher.group(1).length() > 1) {
				//System.out.println(matcher.group(1));
			}
		}
	}

	private static String translateInput(String outputStr, String key) {
		int index = 0;
		int count = 0;
		String output = null;
		
		while (index != -1) {
			index = outputStr.indexOf(key);
			count++;
			if (count % 2 == 0) {
				output = outputStr.substring(index, outputStr.lastIndexOf(key));
			} else {
				output = outputStr.substring(index, outputStr.length()-1);
			}
		}
		System.out.println(output);
		return output;
	}

	private static int translateMagicKeyToLetter(int remainder) {
		if (remainder > 0 && remainder < 27) {
			return remainder + 96;
		} else {
			return remainder + 38;
		}
		
	}
}
