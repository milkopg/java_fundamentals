package com.nakov.courses.java.fundamentals.exams.rectangle;

import java.util.Scanner;

public class Problem2LargestRectangle {

	public static void main(String[] args) {
		String values[][] = fillArrayFromInput();
		markValues(values);
	}

	private static void markValues(String [] []values) {
		
		boolean matrixArray [][] = new boolean[20][20];
		int width = 0;
		int heigth = 0;
		int startRowIndex = 0;
		int startColIndex = 0;
		boolean started = false;
		
		for (int i=0; i < values.length;i++) {
			System.out.println("");
			for (int j=0; j < values[0].length; j++) {
				matrixArray[i][j] = false;
				String currentValue = values[i][j];
				String nextValue = j + 1 < values.length ? values[i][j+1] : null;
				String previousValue = j - 1 >= 0 ? values[i][j-1] : null;
				String previousRowValue = i - 1 > 0 ? values[i-1][j] : null;
				String nextRowValue = i + 1 < values[0].length ? values[i+1][j] : null;
				if (currentValue == null) {
					j = values.length - 1;
					continue;
				}
				if ("END".equals(currentValue)) break;
				
				if (!started) {
					if ((currentValue.equals(nextValue)) && (currentValue.equals(nextRowValue))) {
						width ++;
						matrixArray[i][j]=true;
						System.out.printf("[%s] ", values[i][j]);
						startRowIndex = i;
						startColIndex = j;
						started = true;
					} else {
						System.out.printf("%s ", values[i][j]);
					}
				} else if (currentValue.equals("")) {
					System.out.printf("%s ", values[i][j]);
				} else  {
					if (currentValue.equals(values[i][startColIndex])) {
						width++;
						matrixArray[i][j]=true;
						System.out.printf("[%s] ", values[i][j]);
					} else if (currentValue.equals(values[startRowIndex][startColIndex])) {
						matrixArray[i][j]=true;
						heigth++;
						System.out.printf("[%s] ", values[i][j]);
					} else  {
						System.out.printf("%s ", values[i][j]);
					}
				} 
				
//				if (currentValue.equals("")) {
//					System.out.printf("%s ", values[i][j]);
//				} else if ((currentValue.equals(nextValue)) && (currentValue.equals(nextRowValue)))  {
//					width ++;
//					matrixArray[i][j]=true;
//					System.out.printf("[%s] ", values[i][j]);
//					if (!started) {
//						startRowIndex = i;
//						startColIndex = j;
//					}
//					started = true;
//					
//				} else if (currentValue.equals(values[i][startColIndex + width])) {
//					width++;
//					matrixArray[i][j]=true;
//					System.out.printf("[%s] ", values[i][j]);
//				} else if (currentValue.equals(values[startRowIndex+heigth][startColIndex])) {
//					matrixArray[i][j]=true;
//					heigth++;
//					System.out.printf("[%s] ", values[i][j]);
//				} else  {
//					System.out.printf("%s ", values[i][j]);
//				}
				
//				if (currentValue.equals(nextValue)) {
//					//start of rectangle
//					if (currentValue.equals(nextRowValue)) {
//						width ++;
//						matrixArray[i][j]=true;
//						System.out.printf("[%s] ", values[i][j]);
//						started = true;
//						startRectRowIndex = i;
//						startRectColIndex = j;
//					} else  {
//						matrixArray[i][j]=true;
//						System.out.printf("[%s] ", values[i][j]);
//					}
//				} else if (currentValue.equals(previousRowValue)) {
//					heigth++;
//					matrixArray[i][j]=true;
//					System.out.printf("[%s] ", values[i][j]);
//				} else if  (currentValue.equals(nextRowValue)) {
//					matrixArray[i][j]=true;
//					System.out.printf("[%s] ", values[i][j]);
//					width++;
//				}else {
//					System.out.printf("%s ", values[i][j]);
//				}
//				
				
//				if ((currentValue.equals(nextValue)) && (currentValue.equals(nextRowValue))) {
//					//start of rectangle
//					width ++;
//					matrixArray[i][j]=true;
//					System.out.printf("[%s] ", values[i][j]);
//				} else if ((currentValue.equals(nextValue)) && currentValue.equals(nextValue)) {
//					width++;
//					matrixArray[i][j] = true;
//					System.out.printf("[%s] ", values[i][j]);
//				}else if((currentValue.equals(previousRowValue) && !(currentValue.equals(nextValue)))) {
//					heigth++;
//					matrixArray[i][j]=true;
//					System.out.printf("[%s] ", values[i][j]);
//				} else if ((currentValue.equals(previousValue)) && (currentValue.equals(previousRowValue))) {
//					heigth++;
//					matrixArray[i][j]=true;
//					System.out.printf("[%s] ", values[i][j]);
//				} else {
//					System.out.printf("%s ", values[i][j]);
//				}
//				
//				if (currentValue.equals(currentValue.equals(nextValue)))  {
//					if (currentValue.equals(nextRowValue)) {
//						//begin of rectangle
//						width++;
//						matrixArray[i][j]=true;
//					} else {
//						
//					}
//				} else {
//					System.out.printf("%s ", values[i][j]);
//				}				
				
//				if (currentValue.equals(nextValue) && (currentValue.equals(nextRowValue)) || (currentValue.equals(previousValue) && (currentValue.equals(nextRowValue)))) {
//					width++;
//					System.out.printf("[%s] ", values[i][j]);
//				} else {
//					System.out.printf("%s ", values[i][j]);
//				}
				
//				//values[i][j] = lines[j];
//				if ((j + 1 < values.length)  && (values[i][j].equals(values[i][j+1])) || (j==values.length-1 && values[i][j].equals(values[i][j-1]))) {
//					matrixArray[i][j] = true;
//					System.out.printf("[%s] ", values[i][j]);
//				} else if ((i - 1 >= 0) && (values[i][j].equals(values[i-1][j]) && (matrixArray[i-1][j]))) {
//					matrixArray[i][j] = true;
//					System.out.printf("[%s] ", values[i][j]);
//				} else {
//					System.out.printf("%s ", values[i][j]);
//				}

				//System.out.print(values[i][j] + " ");
				//System.out.printf(values[i][j] + " ");
//				if(!values[i][j].equals("")) {
//					System.out.printf("[%s] ", values[i][j]);
//				}
			}
		}
		//printValues(values, matrixArray);
	}

//	private static void printValues(String[][] values, boolean[][] matrixArray) {
//		for (int i=0; i < values.length)
//		
//	}

	private static String [][] fillArrayFromInput() {
		Scanner scanner = new Scanner(System.in);
		String values[] [] = new String [20][20];
		
		for (int i=0; i < values.length; i++) {
			String line = scanner.hasNextLine() ? scanner.nextLine() : scanner.next();
			if (line.equalsIgnoreCase("END")) {
				break;	
			}
			
//			if (line.startsWith(",")) {
//				line = line.substring(1);
//			}
			
			String lines [] = line.split(",");
			for (int j=0; j < lines.length; j++) {
				values[i][j] = lines[j];
//				if(!values[i][j].equals("")) {
//					System.out.printf("[%s] ", values[i][j]);
//				}
			}
			System.out.println("");
		}
		return values;
	}

}
