package com.nakov.courses.java.fundamentals.exams.rectangle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Problem3LargestRectangle {

	public static void main(String[] args) {
		largestRectangle();
	}

	private static void largestRectangle() {
		Scanner scanner = new Scanner(System.in);
		String line = scanner.nextLine();
		int colums = line.split(",").length;
		int rows = 0;
		String values [][] = new String[20][colums];
		System.out.println();
		List<List<String>> rectangles = new ArrayList<>();
		while (!"END".equals(line))  {
			
			values[rows++] = line.split(",");
			rectangles.add(Arrays.asList(line.split(",")));
//			System.out.println(line);
			line = scanner.nextLine();
		}
		
		findAllRectangles(values, rows);
		
	}

	private static void findAllRectangles(String[][] values, int rows) {
		List<List<Integer>> rectangles = new ArrayList<>();
		int rowIndex=0;
		int colIndex=0;
		for (int i=0; i < rows; i++)  {
			for (int j=0; j < values[0].length; j++) {
				String current = values[i][j];
				if (current == null) break;
				String next = j == values[0].length - 1 ? null : values[i][j+1];
				String previous = j - 1 >= 0 ? values[i][j-1] : null;
				String previousRow = i - 1 > 0 ? values[i-1][j] : null;
				String nextRow = i + 1 < values[0].length ? values[i+1][j] : null;
				
				if (current.equals(next) && current.equals(nextRow)) {
					int rowsInner = i;
					int colsInner = j;
					while (true) {
						if (current.equals(values[i][colsInner])) {
							colIndex = colsInner++;
						} else {
							break;
						}
						
					}
					System.out.println(colIndex);
					while (true){
						if (current.equals(values[rowsInner][j])) {
							rowIndex= rowsInner++;
						} else {
							break;
						}
						
					}
					System.out.println(rowIndex);
					break;
					
//					for (int k=i; k < values.length; k++) {
//						for (int m=j ; m < values[j].length ; m++) {
//							String currentInner = values[k][m];
//							if (currentInner == null) {
//								break;
//							}
//							
//							if (",".equals(currentInner))  {
//								System.out.printf("%-5s", "");
//							}
//							
//							String nextInner = m == values[k].length - 1 ? null : values[k][m+1];
//							String previousInner = m - 1 >= 0 ? values[k][m-1] : null;
//							
//							String nextRowInner = i + 1 < values[k].length ? values[k+1][m] : null;
//							
//							boolean horizontal = false;
//							if (k==i && m == j) {
//								System.out.printf("%-5s", "[" + values[k][m] + "]");
//								horizontal = true;
//							} else if (current.equals(nextInner)) {
//								 if  (current.equals(previousInner)) {
//									System.out.printf("%-5s", "[" + values[k][m] + "]");
//									horizontal = true;
//								} else {
//									System.out.printf("%-5s", values[k][m]);
//								}
//								
//							} else  {
//								if (current.equals(nextRowInner)) {
//									if  (!current.equals(nextInner)) {
//										System.out.printf("%-5s", "[" + values[k][m] + "]");
//									} else {
//										System.out.printf("%-5s", values[k][m]);
//									}
//									
//								} else {
//									System.out.printf("%-5s", values[k][m]);
//								}
//							}  
//							
//							
//						}
//						System.out.println();
//					}
					//break;
				}
			}
		}
	}

}
