package com.nakov.courses.java.fundamentals.exams.rectangle;

import java.util.Arrays;
import java.util.Scanner;

public class Problem3DragonTrap {
	public static void main(String[] args) {
		dragonTrap();
	}

	private static void dragonTrap() {
		Scanner scanner = new Scanner(System.in);
		String line = scanner.nextLine();
		int n = Integer.parseInt(line);
		String lines [] = new String[n];
		String regex = "\\(.*?\\d\\s\\d)\\";
		String [] params = null ;
		char [] [] matrix = new char [n] [];
		int index=0;
		line = scanner.nextLine();
		
		while (!"End".equals(line))  {
			if (line.startsWith("("))  {
				params = (line.replaceAll("[()+]", "").trim().split(" "));
			}
			
			if (index < n) {
				lines[index] = line;
			}
			index++;
			line = scanner.nextLine();
		}
		
		int rowIndex = Integer.parseInt(params[0]);
		int colIndex = Integer.parseInt(params[1]);
		int radius = Integer.parseInt(params[2]);
		int rotations = (int) (Integer.parseInt(params[3]) % Math.pow(2, n));
		
		for  (int i=0; i < lines.length; i++) {
			matrix[i] = lines[i].toCharArray();
		}
		
		//TODO check if rowIndex and colIndex < 0
		
		shiftmatrix(matrix, n, rowIndex, colIndex, radius, rotations);
	}

	private static void shiftmatrix(char[][] matrix, int n, int rowIndex, int colIndex, int radius, int rotations) {
		char [] [] rotatedMatrix =  new char[matrix.length][matrix[0].length] ; Arrays.copyOf(matrix, matrix.length);
		for (int i=0; i < matrix.length; i++) {
			for (int j=0; j < matrix[0].length; j++) {
				rotatedMatrix[i][j] = matrix[i][j];
			}
		}
		
		boolean clockwise = false;
		if (rotations > 0) {
			clockwise = true;
		} else {
			rotations = Math.abs(rotations);
		}
		
		int count = 0;
		for (int i=0; i < rotations; i++) {
				int startRow = rowIndex - radius;
				int startCol = colIndex - radius;
				int endRow = rowIndex + radius;
				int endCol = colIndex + radius;
				int row;
				int col;
					
				if (clockwise) {
					//move right
					for (col = startCol, row = startRow; col < endCol; col++) {
						if (isInsideInMatrix(matrix, row, col, radius)) {
							rotatedMatrix[row][col] = matrix[row][col+1];
							count++;
						}
					}
				
					//move down
					for (row = startRow + 1, col = endCol; row <= endRow; row++) {
						if (isInsideInMatrix(matrix, row, col, radius)) {
							rotatedMatrix[row][col] = matrix[row-1][col];
							count++;
						}
					}
					
					//move left
					for (col = endCol - 1, row = endRow; col >= startCol; col--) {
						if (isInsideInMatrix(matrix, startRow, startCol, radius)) {
							rotatedMatrix[row][col] = matrix[row][col+1];
							count++;
						}
					}
						
					
					//move up
					for (row = endRow - 1, col = startCol; row >= startRow; row--) {
						if (isInsideInMatrix(matrix, row, col, radius)) {
							rotatedMatrix[row][col] = matrix[row+1][col];
							count++;
						}
					}

				} else {
					//move down
					for (row = startRow + 1, col = endCol; row <= endRow; row++) {
						if (isInsideInMatrix(matrix, row, col, radius)) {
							rotatedMatrix[row][col] = matrix[row-1][col];
							count++;
						}
					}
					
					//move right
					for (col = startCol, row = startRow; col < endCol; col++) {
						if (isInsideInMatrix(matrix, row, col, radius)) {
							rotatedMatrix[row][col] = matrix[row][col+1];
							count++;
						}
					}
				
					//move up
					for (row = endRow - 1, col = startCol; row >= startRow; row--) {
						if (isInsideInMatrix(matrix, row, col, radius)) {
							rotatedMatrix[row][col] = matrix[row+1][col];
							count++;
						}
					}
					
					//move left
					for (col = endCol - 1, row = endRow; col >= startCol; col--) {
						if (isInsideInMatrix(matrix, startRow, startCol, radius)) {
							rotatedMatrix[row][col] = matrix[row][col+1];
							count++;
						}
					}
				}
							
				//System.out.println(Arrays.toString(rotatedMatrix));
			}
		printMatrix(rotatedMatrix, count);

	}

	private static void printMatrix(char[][] rotatedMatrix, int count) {
		for (int i = 0; i < rotatedMatrix.length; i++) {
			System.out.println(rotatedMatrix[i]);
		}
		System.out.println("Symbols changed: " + count);
	}

	private static boolean isInsideInMatrix(char[][] matrix, int rowIndex, int colIndex, int radius) {
		return rowIndex>= 0  && colIndex>= 0 && rowIndex < matrix.length && colIndex < matrix[rowIndex].length;
	}
}
