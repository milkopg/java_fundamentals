package com.nakov.courses.java.fundamentals.exams.october;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Problem1TinySporebat {

	public static void main(String[] args) {
		tinySporeBat();
	}

	private static void tinySporeBat() {
		Scanner scanner = new Scanner(System.in);
		String line = scanner.nextLine();
		int startingHeath = 5800;
		int tinySporebatCost = 30;
		long totalGlowcaps = 0;
		Pattern pattern = Pattern.compile("^\\p{ASCII}*$");
		
		while (!line.equals("Sporeggar")) {
			int glowCaps = Character.getNumericValue(line.charAt(line.length()-1));
			char [] enemies = line.substring(0, line.length() - 1).toCharArray();
			for (char enemy : enemies) {
				  if (enemy != 'L') {
					  startingHeath -= enemy;
	                    if (startingHeath <= 0) {
	                        System.out.println("Died. Glowcaps: " + totalGlowcaps);
	                        return;
	                    }
	                } else {
	                	startingHeath += 200;
	                }
			}
			totalGlowcaps += glowCaps;
			line = scanner.nextLine();
		}
		
		System.out.println("Health left: " + startingHeath);
		if (totalGlowcaps >= tinySporebatCost) {
			System.out.println("Bought the sporebat. Glowcaps left: " + (totalGlowcaps - tinySporebatCost));
		} else {
			System.out.printf("Safe in Sporeggar, but another %d Glowcaps needed.", (tinySporebatCost - totalGlowcaps));
		}
	}
}
