package com.nakov.courses.java.fundamentals.exams.rectangle;

import java.util.Scanner;

public class Problem2LargestRectangle2 {

	public static void main(String[] args) {
		String values[][] = fillArrayFromInput();
		markValues(values);
	}

	private static void markValues(String [] []values) {
		
		boolean matrixArray [][] = new boolean[20][20];
//		int rectanglesArray [] [] =  {
//				{1, 2, 3, 4}
//			};
		int [][] rectanglesArray = new int [20][20];  
		int width = 1;
		int heigth = 1;
		int startRowIndex = 0;
		int startColIndex = 0;
		int endRowIndex = 0;
		int endColIndex = 0;
		boolean started = false;
		int foundRectangles=0;
		
		for (int i=0; i < values.length;i++) {
			System.out.println("");
			for (int j=0; j < values[0].length; j++) {
				matrixArray[i][j] = false;
				String currentValue = values[i][j];
				String nextValue = j + 1 < values.length ? values[i][j+1] : null;
				String previousValue = j - 1 >= 0 ? values[i][j-1] : null;
				String previousRowValue = i - 1 > 0 ? values[i-1][j] : null;
				String nextRowValue = i + 1 < values[0].length ? values[i+1][j] : null;
				
				if (currentValue == null) {
					j = values.length - 1;
					continue;
				}
				if ("END".equals(currentValue)) break;
				if (!started) {
					//initial triangle found
					if ((currentValue.equals(nextValue)) && (currentValue.equals(nextRowValue))) {
						startRowIndex = i;
						startColIndex = j;
						foundRectangles++;
						int w=1;
						int h=1;
						int k=0;
						while (k < 0) {
							k=i;
							int m=j;
							
							
							if (currentValue.equals(values[k][m++])) {
								w++;
							} else {
								if (currentValue.equals(values[k++][m])) {
									h++;
								} else {
									rectanglesArray[foundRectangles] = new int [] {startRowIndex, startColIndex, w, h, w * h};
									break;
								}
							}
						}
						
//						for (int k=startRowIndex; k < values.length ; k++) {
//							for (int m = startColIndex; m < values[0].length; m++) {
//								if  ((values[k][m]) == null) continue;
//								String next = m + 1 < values.length ? values[k][m+1] : null;
//								String previous = m - 1 >= 0 ? values[k][m-1] : null;
//								String previousRow = k - 1 > 0 ? values[k-1][m] : null;
//								String nextRow = k + 1 < values[0].length ? values[m+1][j] : null;
//								
//								
//								if (currentValue.equals(next)) {
//									width++;
//								} else if (currentValue.equals(nextRow)) {
//									heigth++;
//								} 
//							}
//						}
						rectanglesArray[foundRectangles] = new int [] {startRowIndex, startColIndex, width, heigth, width * heigth};
						started = false;
					}
				}
				
			}
		}
	}

	private static String [][] fillArrayFromInput() {
		Scanner scanner = new Scanner(System.in);
		String values[] [] = new String [20][20];
		
		for (int i=0; i < values.length; i++) {
			String line = scanner.hasNextLine() ? scanner.nextLine() : scanner.next();
			if (line.equalsIgnoreCase("END")) {
				break;	
			}

			String lines [] = line.split(",");
			for (int j=0; j < lines.length; j++) {
				values[i][j] = lines[j];
			}
			System.out.println("");
		}
		return values;
	}

}
