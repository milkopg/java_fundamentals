package com.nakov.courses.java.fundamentals.lection1.homework;

import java.util.Scanner;

public class Problem8GetAverage {
	
	public static void main(String[] args) {
		readValues();
	}

	private static void readValues() {
		double [] values = new double[3];
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter your 3 numbers for calculation average value: Use delimiter comma!");
		
		for(int i = 0; i < values.length; i++) {
			System.out.println("Enter " + (char)('a' + i) + " digit: ");
			
			if (sc.hasNextDouble()){
				values[i] = sc.nextDouble();
			} else {
				System.out.println("No Valid double values! Exit!");
				return;
			}
			
		}
		sc.close();
		calculateAverageValue(values);
	}

	private static void calculateAverageValue(double[] values) {
		if (values == null) return;
		
		double sum = 0;
		double average;
		for (int i = 0; i < values.length; i++) {
			sum += values[i];
		}
		average = sum / values.length;
		System.out.println(String.format("%.2f", average));
	}
}
