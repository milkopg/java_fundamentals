package com.nakov.courses.java.fundamentals.lection1.homework;

import java.util.Scanner;

public class Problem5PrintCharactersTriangle {


	private static void readerRows() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number or rows: ");
		int number;
		
		try {
			number = Integer.parseInt(sc.next());
		} catch (NumberFormatException nfe) {
			number = 0;
			System.out.println("Invalid number");
		} finally {
			sc.close();
		}
		
		if ((number < 1) || (number > 26)) {
			System.out.println("Number must be between 1 and 26! Exit!");
			return;
		}
		
		//ascending rows
		for (char c = 'a'; c < 'a' + number ; c++) {
			printAlphabet(c);
	    }
	     
		//descending rows
	    for (char c2 = (char) ('a' + number - 2); c2 >= 'a' ; c2--) { 
	    	printAlphabet(c2);
	    }
	    System.out.println();
	}

	private static void printAlphabet(char endChar) {
		for (char c = 'a' ; c <= endChar ; c++) {
			System.out.print(c + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		readerRows();
	}
}
