package com.nakov.courses.java.fundamentals.lection1.homework;

public class Problem4PrintCharacters {

	public static void main(String[] args) {
		for (char c = 'a' ; c <= 'z' ;  c++) System.out.print( c + " " );
	}
}
