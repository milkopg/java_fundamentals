package com.nakov.courses.java.fundamentals.lection1.homework;

import java.util.Scanner;

public class Problem7GhettoNumeralSystem {

	private static final String [] values = {
		"Gee", "Bro", "Zuz", "Ma", "Duh", "Yo",  "Dis", "Hood", "Jam", "Mack"
	} ;
	
	public static void main(String[] args) {
		readValues();
	}

	private static void readValues() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your digits: ");
		String inputString = sc.next();

		if (inputString.matches("\\d+")) {
			splitDigitValues(inputString);
		} else {
			System.out.println("Wrong input! Allowed characters must be only digits from 0 to 9");
		}
		sc.close();
	}

	private static void splitDigitValues(String numberString) {
		for (int i=0; i < numberString.length(); i++) {
			int index = Character.digit(numberString.charAt(i), 10);
			System.out.print(values[index]);
		}
	}
}
