package com.nakov.courses.java.fundamentals.lection1.homework;

import java.util.Scanner;

public class Problem6SumRangeOfNumbers {
	public static void main(String[] args) {
		sumRangeNumbers();
	}

	private static void sumRangeNumbers() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your digit to sum: ");
		int number;
		
		try {
			number = Integer.parseInt(sc.next());
			if(checkNumber(number)) {
				makeAndPrintCalculation(number);
			}
		} catch (NumberFormatException nfe) {
			System.out.println("Invalid number");
			return;
		} finally {
			sc.close();
		}
	}

	private static boolean checkNumber(int number) {
		if (number <=0 ) {
			System.out.println("Number must be a positive value. Exiting!");
			return false;
		}
		return true;
	}

	private static void makeAndPrintCalculation(int number) {
		int sum = 0;
		for (int i=1; i <= number; i++) {
			sum = sum + i;
		}
		System.out.println(sum);
	}
}
